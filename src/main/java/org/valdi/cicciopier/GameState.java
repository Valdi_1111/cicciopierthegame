package org.valdi.cicciopier;

public enum GameState {
    /**
     * The user is choosing a level
     */
    SELECTING_LEVEL,
    /**
     * The game is running
     */
    RUNNING,
    /**
     * The game is paused
     */
    PAUSED,
    /**
     * The player has finished the level.
     */
    WON,
    /**
     * The player is currently dead. We are waiting for a choice.
     */
    DEAD,
    /**
     * The player is currently dead and his lives counter is 0.
     */
    FULL_DEAD,
    /**
     * The level will be reset in the next tick.
     */
    WAITING_RESET,
    /**
     * The level will be full reset in the next tick.
     */
    WAITING_FULL_RESET,
    /**
     * The level is resetting
     */
    RESETTING,
    /**
     * The game is stopping
     */
    STOPPING;
}
