package org.valdi.cicciopier;

import javafx.scene.canvas.GraphicsContext;

public interface IRenderizable {

    void doRender(GraphicsContext g);

}
