package org.valdi.cicciopier.world;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.valdi.cicciopier.Checkpoint;
import org.valdi.cicciopier.Location;

import java.util.List;

public class LevelProperties {
    @Expose @SerializedName("level-name")
    private String name;
    @Expose
    private String background;
    @Expose @SerializedName("spawn-point")
    private Location spawnpoint;
    @Expose @SerializedName("check-points")
    private List<Checkpoint> checkpoints;
    @Expose @SerializedName("end-point")
    private Location endpoint;

    public String getName() {
        return name;
    }

    public String getBackground() {
        return background;
    }

    public Location getSpawnpoint() {
        return spawnpoint;
    }

    public List<Checkpoint> getCheckpoints() {
        return checkpoints;
    }

    public Location getEndpoint() {
        return endpoint;
    }

    @Override
    public String toString() {
        return "LevelProperties{" +
                "name='" + name + '\'' +
                ", background='" + background + '\'' +
                ", spawnpoint=" + spawnpoint +
                ", checkpoints=" + checkpoints +
                '}';
    }
}
