package org.valdi.cicciopier.world;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.stream.JsonReader;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.valdi.cicciopier.*;
import org.valdi.cicciopier.assets.Textures;
import org.valdi.cicciopier.block.Block;
import org.valdi.cicciopier.block.IBlockData;
import org.valdi.cicciopier.entity.Entity;
import org.valdi.cicciopier.entity.Player;
import org.valdi.cicciopier.key.BlocksKey;
import org.valdi.cicciopier.key.EntitiesKey;
import org.valdi.cicciopier.registries.Registries;

import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import static org.valdi.cicciopier.GameDetails.*;

public class World implements ITickable, IRenderizable {
    private static final Logger LEVEL_LOGGER = LogManager.getLogger("Level");

    private final Map<Location, IBlockData> blocks;
    private final List<Entity> entities;

    private final LevelProperties properties;
    private File folder;
    private Image background;

    private final Game game;
    private double offset;

    public World(Game game, LevelProperties properties) throws CorruptedWorldException {
        this.blocks = new ConcurrentHashMap<>();
        this.entities = new CopyOnWriteArrayList<>();

        this.properties = properties;

        this.game = game;
        this.offset = 0D;
    }

    public void loadFromFiles(File folder) {
        this.folder = folder;
        Gson gson = new Gson();

        LEVEL_LOGGER.info("Loading level background...");
        File backgroundFile = new File(folder, properties.getBackground());
        try (FileInputStream fis = new FileInputStream(backgroundFile)) {
            this.background = new Image(fis);
        } catch (IOException e) {
            throw new CorruptedWorldException("Exception thrown while reading background file", e);
        }

        LEVEL_LOGGER.info("Loading level blocks...");
        File blocksFile = new File(folder, "blocks.json");
        try (FileReader fr = new FileReader(blocksFile);
             JsonReader reader = new JsonReader(fr)) {
            reader.beginObject();
            while (reader.hasNext()) {
                String key = reader.nextName();
                String value = reader.nextString();

                String[] coords = key.split("x", 2);
                String[] rangeX = coords[0].split("-", 2);
                String[] rangeY = coords[1].split("-", 2);

                int x2, x1 = Integer.parseInt(rangeX[0]);
                if (rangeX.length > 1) {
                    x2 = Integer.parseInt(rangeX[1]);
                } else {
                    x2 = x1;
                }

                int y2, y1 = Integer.parseInt(rangeY[0]);
                if (rangeY.length > 1) {
                    y2 = Integer.parseInt(rangeY[1]);
                } else {
                    y2 = y1;
                }

                String blockId = value;
                String json = "";
                if(value.contains(" ")) {
                    int index = value.indexOf(' ');
                    blockId = value.substring(0, index);
                    json = value.substring(index + 1);
                }

                BlocksKey bk = new BlocksKey(blockId);
                for (int coordY = y1; coordY <= y2; coordY++) {
                    for (int coordX = x1; coordX <= x2; coordX++) {
                        Optional<Block> block = Registries.BLOCKS.getOptional(bk);
                        if (!block.isPresent()) {
                            LEVEL_LOGGER.info("Invalid block {} at x: {}, y: {}", blockId, coordX, coordY);
                            continue;
                        }

                        //LEVEL_LOGGER.info("Adding block {} at x: {}, y: {}", blockId, coordX, coordY);
                        IBlockData blockData = this.addBlock(block.get(), new Location(coordX, coordY));
                        block.get().fromLoad(blockData, gson.fromJson(json, JsonObject.class));
                    }
                }
            }
            reader.endObject();
        } catch (IOException e) {
            throw new CorruptedWorldException("Exception thrown while parsing blocks.json", e);
        }

        LEVEL_LOGGER.info("Loading level entities...");
        File entitiesFile = new File(folder, "entities.json");
        try (FileReader fr = new FileReader(entitiesFile);
             JsonReader reader = new JsonReader(fr)) {
            reader.beginObject();
            while (reader.hasNext()) {
                String key = reader.nextName();
                String value = reader.nextString();

                String[] coords = key.split("x", 2);
                int coordX = Integer.parseInt(coords[0]);
                int coordY = Integer.parseInt(coords[1]);

                String entityId = value;
                String json = "";
                if(value.contains(" ")) {
                    int index = value.indexOf(' ');
                    entityId = value.substring(0, index);
                    json = value.substring(index + 1);
                }

                EntitiesKey ek = new EntitiesKey(entityId);
                Optional<Class<? extends Entity>> entityClazz = Registries.ENTITIES.getOptional(ek);
                if (!entityClazz.isPresent()) {
                    LEVEL_LOGGER.info("Found invalid entity {} at x: {}, y: {}", entityId, coordX, coordY);
                    continue;
                }

                //LEVEL_LOGGER.info("Adding entity {} at x: {}, y: {}", entityId, coordX, coordY);
                Optional<? extends Entity> entity = this.addEntity(entityClazz.get());
                if(!entity.isPresent()) {
                    LEVEL_LOGGER.info("Unable to spawn entity!");
                    continue;
                }

                entity.get().setX(coordX * getBlockSize());
                entity.get().setY(coordY * getBlockSize());

                entity.get().fromLoad(gson.fromJson(json, JsonObject.class));
            }
            reader.endObject();
        } catch (IOException e) {
            throw new CorruptedWorldException("Exception thrown while parsing entities.json", e);
        }
    }

    public IBlockData addBlock(Block block, Location loc) {
        IBlockData blockData = block.createBlockData(this);
        blockData.setBlockX(loc.getX());
        blockData.setBlockY(loc.getY());

        blocks.put(loc, blockData);
        return blockData;
    }

    public <T extends Entity> Optional<T> addEntity(Class<T> clazz) {
        try {
            Constructor<T> constructor = clazz.getConstructor(World.class);
            T entity = constructor.newInstance(this);
            entities.add(entity);
            return Optional.of(entity);
        } catch (NoSuchMethodException e) {
            LEVEL_LOGGER.error("Unable to create entity, no constructor has been found!", e);
        } catch (IllegalAccessException e) {
            LEVEL_LOGGER.error("Unable to create entity, the constructor may be private or protected!", e);
        } catch (InstantiationException e) {
            LEVEL_LOGGER.error("Unable to create entity, the class may be abstract, interface, array or primitive!", e);
        } catch (InvocationTargetException e) {
            LEVEL_LOGGER.error("Unable to create entity, error thrown while initializing constructor!", e);
        }
        return Optional.empty();
    }

    @Override
    public void doTick() {
        Iterator<IBlockData> ib = blocks.values().iterator();
        while (ib.hasNext()) {
            IBlockData b = ib.next();
            if (b.isRemove()) { // Block has to be removed.
                ib.remove();
                continue;
            }

            // Do block tick.
            b.doTick();
        }

        Iterator<Entity> ie = entities.iterator();
        entities.forEach(e -> {
            if (e.isRemove()) { // Entity has to be removed.
                entities.remove(e);
                return;
            }

            // Do entity tick.
            e.doTick();
        });
    }

    @Override
    public void doRender(GraphicsContext g) {
        if (background != null) {
            g.drawImage(background, offset, 0D, 1280D, 720D, 0D, 0D, WIDTH, HEIGHT);
        }

        blocks.forEach((l, b) -> b.doRender(g));
        entities.forEach(e -> e.doRender(g));

        properties.getCheckpoints().forEach(c -> {
            Location loc = c.getLocation();
            g.drawImage(Textures.ICON_CHECKPOINT.getTexture(), loc.getX() * getBlockSize() - offset, loc.getY() * getBlockSize(), getBlockSize(), getBlockSize() * 2);
        });

        Location loc = properties.getEndpoint();
        g.drawImage(Textures.ICON_ENDPOINT.getTexture(), loc.getX() * getBlockSize() - offset, loc.getY() * getBlockSize(), getBlockSize(), getBlockSize() * 2);
    }

    public IBlockData getBlockAt(double x, double y) {
        double size = getBlockSize();
        int xLoc = (int) Math.floor(x / size);
        int yLoc = (int) Math.floor(y / size);

        Location loc = new Location(xLoc, yLoc);
        //LogManager.getLogger("Block Calculator").info("x: {}, y: {}, xLoc: {}, yLoc: {}", x, y, xLoc, yLoc);
        return blocks.getOrDefault(loc, null);
    }

    // 0-31 -> 0
    // 32-63 -> 1
    // 64-95 -> 2
    // 96-127 -> 3
    // 128-159 -> 4
    // 160-191 -> 5
    public List<IBlockData> getBlocksDownAt(double x, double y) {
        double size = getBlockSize();
        int xLoc = (int) Math.floor(x / size);
        int yLoc = (int) Math.floor(y / size);

        Location loc = new Location(xLoc, yLoc);
        Location locAfter = new Location(xLoc + 1, yLoc);
        //LogManager.getLogger("Block Calculator").info("x: {}, y: {}, xLoc: {}, yLoc: {}", x, y, xLoc, yLoc);

        List<IBlockData> blockz = new ArrayList<>();
        blockz.add(blocks.getOrDefault(loc, null));
        blockz.add(blocks.getOrDefault(locAfter, null));
        return blockz;
    }

    public List<IBlockData> getBlocksSideAt(double x, double yUp, double yDown) {
        double size = getBlockSize();
        int xLoc = (int) Math.floor(x / size);
        int yLocUp = (int) Math.floor(yUp / size);
        int yLocMid = (int) Math.floor(((yUp + yDown) / 2) / size);
        int yLocDown = (int) Math.floor(yDown / size);

        Location locUp = new Location(xLoc, yLocUp);
        Location locMid = new Location(xLoc, yLocMid);
        Location locDown = new Location(xLoc, yLocDown);

        List<IBlockData> blockz = new ArrayList<>();
        blockz.add(blocks.getOrDefault(locUp, null));
        blockz.add(blocks.getOrDefault(locMid, null));
        blockz.add(blocks.getOrDefault(locDown, null));
        return blockz;
    }

    public LevelProperties getProperties() {
        return properties;
    }

    public File getFolder() {
        return folder;
    }

    public void updateOffset(double xDelta) {
        this.offset += xDelta;
    }

    public double getOffset() {
        return offset;
    }

    public List<Entity> getEntities() {
        return entities;
    }

    public Map<Location, IBlockData> getBlocks() {
        return blocks;
    }

    public Optional<Player> getPlayer() {
        return entities.stream()
                .filter(e -> e.getClass().isAssignableFrom(Player.class))
                .map(e -> (Player) e).findFirst();
    }
}
