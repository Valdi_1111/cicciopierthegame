package org.valdi.cicciopier.world;

public class CorruptedWorldException extends RuntimeException {
    public CorruptedWorldException(String message, Throwable cause) {
        super(message, cause);
    }

    public CorruptedWorldException(Throwable cause) {
        super(cause);
    }
}
