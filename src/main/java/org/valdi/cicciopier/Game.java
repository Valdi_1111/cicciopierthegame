package org.valdi.cicciopier;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import javafx.application.Platform;
import javafx.scene.control.Button;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.layout.PatternLayout;
import org.valdi.SuperApiX.common.config.FilesProvider;
import org.valdi.SuperApiX.common.config.IFilesProvider;
import org.valdi.SuperApiX.common.config.serializers.SetSerializer;
import org.valdi.SuperApiX.common.databases.DatabasesProvider;
import org.valdi.SuperApiX.common.databases.IDatabasesProvider;
import org.valdi.SuperApiX.common.dependencies.IDependencyManager;
import org.valdi.SuperApiX.common.dependencies.UniversalDependencyManager;
import org.valdi.SuperApiX.common.logging.CompatibilityLogger;
import org.valdi.SuperApiX.common.logging.SuperLogger;
import org.valdi.SuperApiX.common.plugin.StoreLoader;
import org.valdi.SuperApiX.common.scheduler.SimpleScheduler;
import org.valdi.cicciopier.assets.*;
import org.valdi.cicciopier.block.Block;
import org.valdi.cicciopier.block.Blocks;
import org.valdi.cicciopier.entity.Entities;
import org.valdi.cicciopier.entity.LivingEntity;
import org.valdi.cicciopier.entity.Player;
import org.valdi.cicciopier.input.KeyInputController;
import org.valdi.cicciopier.input.listeners.PressKeyListener;
import org.valdi.cicciopier.registries.*;
import org.valdi.cicciopier.window.GameWindow;
import org.valdi.cicciopier.world.LevelProperties;
import org.valdi.cicciopier.world.World;

import java.io.*;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;

import static org.valdi.cicciopier.GameDetails.*;

public class Game implements StoreLoader {
    private static org.apache.logging.log4j.Logger logger = LogManager.getLogger("Game");
    private static Game INSTANCE;

    static {
        org.apache.logging.log4j.core.Logger rootLogger = (org.apache.logging.log4j.core.Logger) LogManager.getRootLogger();
        PatternLayout layout = PatternLayout.newBuilder().withPattern("[%d{HH:mm:ss}] [%t/%level] [%logger{36}]: %msg%n").build();
        TextAreaAppender appender = TextAreaAppender.createAppender("Gui", false, layout, null);
        appender.start();
        rootLogger.getContext().getConfiguration().addLoggerAppender(rootLogger, appender);
    }

    private final File jarFile;
    private final SuperLogger simpleLogger;
    private final UniversalDependencyManager dependencyManager;

    private final ScheduledExecutorService executors;
    private final SimpleScheduler scheduler;

    private final DatabasesProvider databasesProvider;
    private final FilesProvider filesProvider;

    private GameThread thread;
    private RenderTimer renderer;

    private final KeyInputController keyController;
    private Map<String, World> worlds;

    private GameState state;
    private String level;
    private int lives;
    private int checkpoint;
    private int coins;

    private final File levelsFolder;
    private final File resourcepacksFolder;

    public Game(String[] args) throws RuntimeException {
        try {
            this.jarFile =  new File(Game.class.getProtectionDomain().getCodeSource().getLocation().toURI());
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }

        logger.info("Initializing logger...");
        this.simpleLogger = new CompatibilityLogger(logger);
        logger.info("Initializing dependency manager...");
        this.dependencyManager = UniversalDependencyManager.init(this);
        logger.info("Loading dependencies...");
        Dependencies.loadAll(dependencyManager);

        logger.info("Initializing thread pool...");
        this.executors = Executors.newScheduledThreadPool(10, new ThreadFactoryBuilder().setNameFormat("Scheduled Thread #%d").build());
        logger.info("Initializing scheduler...");
        this.scheduler = new SimpleScheduler(this, "Async Thread #%d");

        logger.info("Initializing databases provider...");
        this.databasesProvider = new DatabasesProvider();
        logger.info("Initializing files provider...");
        this.filesProvider = new FilesProvider();

        // Register common serializers
        new SetSerializer().register();

        this.keyController = new KeyInputController();
        this.worlds = new HashMap<>();

        this.state = GameState.SELECTING_LEVEL;
        this.lives = DEFAULT_LIVES;
        this.checkpoint = 0;
        this.coins = 0;

        this.keyController.registerListener(new PressKeyListener(KeyCode.SPACE) {
            @Override
            protected void press(KeyEvent e) {
                if(state != GameState.RUNNING) {
                    return;
                }

                Optional<Player> player = getPlayer();
                player.ifPresent(LivingEntity::jump);
            }
        });

        this.keyController.registerListener(new PressKeyListener(KeyCode.ESCAPE) {
            @Override
            protected void press(KeyEvent e) {
                if(state == GameState.RUNNING) {
                    logger.info("Pausing game...");
                    state = GameState.PAUSED;
                }
                else if(state == GameState.PAUSED) {
                    logger.info("Unpausing game...");
                    state = GameState.RUNNING;
                }
            }
        });

        logger.info("Registering blocks...");
        register(Blocks.class);
        logger.info("Registering textures...");
        register(Textures.class);
        logger.info("Registering sounds...");
        register(Sounds.class);
        logger.info("Registering entities...");
        register(Entities.class);

        this.resourcepacksFolder = new File(this.getDataFolder(), "resourcepacks");
        logger.info("Loading textures...");
        loadTextures();
        logger.info("Loading sounds...");
        loadSounds();

        logger.info("Loading levels...");
        this.levelsFolder = new File(this.getDataFolder(), "levels");
        copyDefaultLevel();
        loadLevels();

        INSTANCE = this;
    }

    public <T extends AutoRegister> void register(Class<T>... classes) {
        for(Class<T> clazz : classes) {
            Field[] fields = clazz.getDeclaredFields();
            for(Field field : fields) {
                if(!Modifier.isStatic(field.getModifiers())) {
                    continue;
                }

                if(Block.class.isAssignableFrom(field.getType())) {
                    try {
                        field.setAccessible(true);
                        Block block = (Block) field.get(null);
                        logger.info("Registering block {}", block.getKey());
                        Registries.BLOCKS.register(block);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                if(ITexture.class.isAssignableFrom(field.getType())) {
                    try {
                        field.setAccessible(true);
                        ITexture texture = (ITexture) field.get(null);
                        logger.info("Registering texture {}", texture.getKey());
                        Registries.TEXTURES.register(texture);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                if(ISound.class.isAssignableFrom(field.getType())) {
                    try {
                        field.setAccessible(true);
                        ISound sound = (ISound) field.get(null);
                        logger.info("Registering sound {}", sound.getKey());
                        Registries.SOUNDS.register(sound);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                if(Entities.EntityEntry.class.isAssignableFrom(field.getType())) {
                    try {
                        field.setAccessible(true);
                        Entities.EntityEntry entity = (Entities.EntityEntry) field.get(null);
                        logger.info("Registering entity {}", entity.getKey());
                        Registries.ENTITIES.register(entity.getKey(), entity.getClazz());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public void loadTextures() {
        Registries.TEXTURES.forEach(t -> t.load(this, null));
    }

    public void loadSounds() {
        Registries.SOUNDS.forEach(t -> t.load(this, null));
    }

    /**
     * Copy default level if doesn't exists.
     */
    private void copyDefaultLevel() {
        File defaultLevelFolder = new File(levelsFolder, "default");
        for(String file : new String[] {"background.jpg", "blocks.json", "entities.json", "properties.json"}) {
            File outFile = new File(defaultLevelFolder, file);
            //try (InputStream is = Game.class.getClassLoader().getResourceAsStream("levels/default/" + file)) {
            try (InputStream is = getResource("levels/default/" + file)) {
                // Make any dirs that need to be made
                if (!outFile.exists()) {
                    outFile.getParentFile().mkdirs();
                    Files.copy(is, outFile.toPath());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Load all levels in the level folder.
     */
    public void loadLevels() {
        File[] files = this.levelsFolder.listFiles();
        if(files == null) {
            return;
        }

        for(File levelFolder : files) {
            if(!levelFolder.isDirectory()) {
                continue;
            }

            File propertiesFile = new File(levelFolder, "properties.json");
            if(!propertiesFile.exists()) {
                continue;
            }

            logger.info("Fetching level properties for level at folder {}...", propertiesFile.getParentFile().getName());
            try (FileReader fr = new FileReader(propertiesFile);
                 JsonReader reader = new JsonReader(fr)) {
                Gson gson = new Gson();
                LevelProperties properties = gson.fromJson(reader, LevelProperties.class);
                this.loadLevel(properties, levelFolder);
            } catch (IOException e) {
                logger.error("Unable to read level properties... Level won't be loaded!");
            }
        }
    }

    /**
     * Load level from files.
     * @param properties the level's props
     * @param folder the level's folder
     */
    public void loadLevel(LevelProperties properties, File folder) {
        // Create world
        World world = new World(this, properties);
        world.loadFromFiles(folder);

        this.worlds.put(world.getProperties().getName(), world);
    }

    /**
     * Reset the current playing level.
     */
    public void resetLevel(boolean all) {
        Optional<World> oldLevel = getPlayingLevel();
        if(!oldLevel.isPresent()) {
            return;
        }

        // Game has been resetted.
        this.state = GameState.RESETTING;

        logger.info("Resetting level {}...", level);
        this.coins = 0;

        if(all) {
            this.checkpoint = 0;
        }

        loadLevel(oldLevel.get().getProperties(), oldLevel.get().getFolder());
        startLevel(level);
    }

    public void startLevel(String level) {
        Optional<World> world = getLevel(level);
        if(!world.isPresent()) {
            return;
        }

        this.level = level;

        Optional<Player> player = world.get().addEntity(Player.class);
        if(!player.isPresent()) {
            return;
        }

        Location spawn = world.get().getProperties().getSpawnpoint();
        if(checkpoint > 0) {
            Optional<Checkpoint> cp = world.get().getProperties().getCheckpoints().stream().filter(c -> checkpoint == c.getId()).findFirst();
            if(cp.isPresent()) {
                spawn = cp.get().getLocation();
                world.get().updateOffset((spawn.getX() - 2) * getBlockSize());
            }
        }

        player.get().setX(spawn.getX() * getBlockSize());
        player.get().setY(spawn.getY() * getBlockSize());

        this.state = GameState.RUNNING;
    }

    public void endLevel() {
        if(state != GameState.RUNNING) {
            return;
        }

        this.state = GameState.WON;

        Platform.runLater(() -> {
            Button restart = (Button) renderer.getWindow().getScene().lookup("#restart");
            restart.setVisible(true);
        });
    }

    /**
     * Start the game.
     * @param level the level to play
     * @param window the game window
     */
    public void startGame(String level, GameWindow window) {
        if(state != GameState.SELECTING_LEVEL) {
            return;
        }

        // Start render thread.
        logger.info("Starting Render Thread...");
        this.renderer = new RenderTimer(this, window);
        renderer.start();

        window.getScene().setOnKeyPressed(keyController::keyPressed);
        window.getScene().setOnKeyReleased(keyController::keyReleased);
        window.getScene().setOnKeyTyped(keyController::keyTyped);

        window.widthProperty().addListener((observable, oldValue, newValue) -> WIDTH = newValue.intValue());
        window.heightProperty().addListener((observable, oldValue, newValue) -> HEIGHT = newValue.intValue());

        // Start scheduler.
        logger.info("Starting scheduler...");
        SimpleScheduler.startAcceptingTasks();

        // Start main thread.
        logger.info("Starting Game Thread...");
        this.thread = new GameThread(this);
        thread.start();

        this.startLevel(level);
    }

    /**
     * Stop the game.
     */
    public synchronized void stopGame() {
        if(state != GameState.RUNNING) {
            return;
        }

        // Game has been stopped.
        this.state = GameState.STOPPING;

        // Stop scheduler.
        logger.info("Shutting down scheduler...");
        scheduler.shutdown();

        // Stop for Render Thread.
        logger.info("Shutting down Render Thread...");
        renderer.stop();

        // Wait for Game Thread.
        try {
            logger.info("Shutting down Game Thread...");
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        logger.info("Closing application...");
        Platform.exit();
        System.exit(0);
    }

    public static Game getInstance() {
        return INSTANCE;
    }

    public KeyInputController getKeyController() {
        return keyController;
    }

    public String getLevel() {
        return level;
    }

    public GameState getState() {
        return state;
    }

    public void setState(GameState state) {
        this.state = state;
    }

    /*@Deprecated
    public boolean isRunning() {
        return state == GameState.RUNNING;
    }*/

    public Optional<Player> getPlayer() {
        return getPlayingLevel().flatMap(World::getPlayer);
    }

    public int getCheckpoint() {
        return checkpoint;
    }

    public void setCheckpoint(int checkpoint) {
        this.checkpoint = checkpoint;
    }

    public void decrementLives() {
        lives--;
        state = GameState.DEAD;
        if(lives == 0) {
            lives = DEFAULT_LIVES;
            state = GameState.FULL_DEAD;
        }

        Platform.runLater(() -> {
            Button restart = (Button) renderer.getWindow().getScene().lookup("#restart");
            restart.setVisible(true);
        });
    }

    public int getLives() {
        return lives;
    }

    public void setLives(int lives) {
        this.lives = lives;
    }

    public int getCoins() {
        return coins;
    }

    public void setCoins(int coins) {
        this.coins = coins;
    }

    public void incrementCoins(int amount) {
        coins += amount;
    }

    public Optional<World> getPlayingLevel() {
        return getLevel(level);
    }

    public Optional<World> getLevel(String level) {
        return Optional.ofNullable(worlds.getOrDefault(level, null));
    }

    public Collection<World> getLevels() {
        return worlds.values();
    }

    public int getLastTps() {
        return thread.getLastTps();
    }

    public int getCurrentTicks() {
        return thread.getCurrentTicks();
    }

    public float getFps() {
        return renderer.getFps();
    }

    public File getLevelsFolder() {
        return levelsFolder;
    }

    public File getResourcepacksFolder() {
        return resourcepacksFolder;
    }

    @Override
    public String getName() {
        return GameDetails.NAME;
    }

    @Override
    public String getVersion() {
        return GameDetails.VERSION;
    }

    @Override
    public List<String> getAuthors() {
        return Arrays.asList(GameDetails.AUTHORS);
    }

    @Override
    public SuperLogger getLogger() {
        return this.simpleLogger;
    }

    @Override
    public SimpleScheduler getScheduler() {
        return this.scheduler;
    }

    @Override
    public File getDataFolder() {
        return jarFile.getParentFile();
    }

    @Override
    public File getJarFile() {
        return this.jarFile;
    }

    @Override
    public ClassLoader getJarLoader() {
        return Game.class.getClassLoader();
    }

    @Override
    public InputStream getResource(String path) {
        if (path == null || path.isEmpty()) {
            throw new IllegalArgumentException("Filename cannot be null or empty!");
        }

        try {
            URL url = getJarLoader().getResource(path);

            if (url == null) {
                return null;
            }

            URLConnection connection = url.openConnection();
            connection.setUseCaches(false);
            return connection.getInputStream();
        } catch (IOException ex) {
            return null;
        }
    }

    @Override
    public void saveResource(String resourcePath, boolean replace) {
        if (resourcePath == null || resourcePath.equals("")) {
            throw new IllegalArgumentException("ResourcePath cannot be null or empty");
        }

        resourcePath = resourcePath.replace('\\', '/');
        try (InputStream in = getResource(resourcePath)) {
            if (in == null) {
                throw new IllegalArgumentException("The embedded resource '" + resourcePath + "' cannot be found in " + getJarFile());
            }

            File outFile = new File(getDataFolder(), resourcePath);
            // Make any dirs that need to be made
            outFile.getParentFile().mkdirs();
            if (!outFile.exists() || replace) {
                Files.copy(in, outFile.toPath());
            }
        } catch (IOException e) {
            logger.error("Could not save from jar file. " + resourcePath, e);
        }
    }

    @Override
    public ThreadFactory getThreadFactory() {
        ThreadPoolExecutor threadPoolExecutor = (ThreadPoolExecutor) this.getExecutorService();
        return threadPoolExecutor.getThreadFactory();
    }

    @Override
    public ScheduledExecutorService getExecutorService() {
        return this.executors;
    }

    @Override
    public IDependencyManager getDependencyManager() {
        return dependencyManager;
    }

    @Override
    public Optional<IDatabasesProvider> getDatabasesProvider() {
        return Optional.of(databasesProvider);
    }

    @Override
    public Optional<IFilesProvider> getFilesProvider() {
        return Optional.of(filesProvider);
    }
}
