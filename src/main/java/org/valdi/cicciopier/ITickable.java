package org.valdi.cicciopier;

public interface ITickable {

    void doTick();

}
