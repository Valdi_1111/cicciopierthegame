package org.valdi.cicciopier.entity;

import org.valdi.cicciopier.world.World;

import java.util.Optional;

import static org.valdi.cicciopier.GameDetails.WIDTH;
import static org.valdi.cicciopier.GameDetails.getBlockSize;

public class Cipallottola extends Enemy {
    private static final int SPEED = 3;

    public Cipallottola(World world) {
        super(world);

        setMaxHealth(100D);
        setHealth(100D);

        setWidth(getBlockSize());
        setHeight(getBlockSize());

        setDamage(25D);
    }

    @Override
    public String getTexture() {
        return "entity.cipallottola.still";
    }

    @Override
    public String getTextureMove() {
        return "entity.cipallottola.move";
    }

    @Override
    protected void tickAI() {
        Optional<Player> player = world.getPlayer();
        if(!player.isPresent()) {
            return;
        }

        double distance = (player.get().getX() + player.get().getWidth() / 2) - (posX + width / 2);
        if(distance > 10) {
            speedX = SPEED;
        }
        else if(distance < -10) {
            speedX = -SPEED;
        }
        else {
            speedX = 0;
        }

        damagePlayer(player.get(), false);
    }
}
