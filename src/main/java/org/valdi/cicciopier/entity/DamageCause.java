package org.valdi.cicciopier.entity;

public enum DamageCause {
    ENEMY,
    FALL,
    VOID;
}
