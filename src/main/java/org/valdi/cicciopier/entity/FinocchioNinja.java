package org.valdi.cicciopier.entity;

import org.valdi.cicciopier.world.World;

import java.util.Optional;

import static org.valdi.cicciopier.GameDetails.WIDTH;
import static org.valdi.cicciopier.GameDetails.getBlockSize;

public class FinocchioNinja extends Enemy {
    protected long lastTrown;

    public FinocchioNinja(World world) {
        super(world);

        setMaxHealth(100D);
        setHealth(100D);

        setWidth(getBlockSize() * 2);
        setHeight(getBlockSize() * 2);

        this.lastTrown = System.currentTimeMillis();
    }

    @Override
    public String getTexture() {
        return "entity.finocchio_ninjia.still";
    }

    @Override
    public String getTextureMove() {
        return "entity.finocchio_ninjia.move";
    }

    @Override
    protected void tickAI() {
        Optional<Player> player = world.getPlayer();
        if(!player.isPresent()) {
            return;
        }

        double distance = (player.get().getX() + player.get().getWidth() / 2) - (posX + width / 2);
        if(distance > 10) {
            prevDirection = true;
        }
        else if(distance < -10) {
            prevDirection = false;
        }

        long now = System.currentTimeMillis();
        if(now - lastDamage < 2000L) {
            return;
        }

        this.lastDamage = now;
        if(Math.abs(distance) > WIDTH / 2D) {
            return;
        }

        Optional<Shuriken> proj = world.addEntity(Shuriken.class);
        if(!proj.isPresent()) {
            return;
        }

        proj.get().setX(posX + width / 2);
        proj.get().setY(posY + height / 2 - getBlockSize() / 4D);

        proj.get().setDirection(distance > 1);
    }
}
