package org.valdi.cicciopier.entity;

import javafx.geometry.BoundingBox;
import javafx.geometry.Point2D;
import org.valdi.cicciopier.Checkpoint;
import org.valdi.cicciopier.Game;
import org.valdi.cicciopier.Location;
import org.valdi.cicciopier.block.IBlockData;
import org.valdi.cicciopier.input.Key;
import org.valdi.cicciopier.world.World;

import java.util.List;

import static org.valdi.cicciopier.GameDetails.*;

public class Player extends LivingEntity {

    public Player(World world) {
        super(world);

        setMaxHealth(300D);
        setHealth(300D);

        setWidth(getBlockSize());
        setHeight(getBlockSize() * 2 - 4);
    }

    @Override
    public void doTick() {
        super.doTick();
        this.speedX = 0;

        if (Key.RIGHT.isDown() && !Key.LEFT.isDown()) {
            runRight();
        }
        if (!Key.RIGHT.isDown() && Key.LEFT.isDown()) {
            runLeft();
        }
    }

    @Override
    protected void tickMovement() {
        super.tickMovement();

        Game game = Game.getInstance();
        Location end = world.getProperties().getEndpoint();
        if(posX > end.getX() * getBlockSize()) {
            game.endLevel();
            return;
        }

        List<Checkpoint> checks = world.getProperties().getCheckpoints();
        checks.forEach(check -> {
            if(game.getCheckpoint() >= check.getId()) {
                return;
            }

            if(posX > check.getLocation().getX() * getBlockSize()) {
                game.setCheckpoint(check.getId());
            }
        });
    }

    @Override
    protected double checkHorizontalCollisions(double xDelta) {
        if(xDelta == 0) {
            return 0;
        }

        BoundingBox bbox = this.getBoundingBox();
        if (xDelta > 0) {
            // Check for right collisions
            for (IBlockData b : world.getBlocksSideAt(posX + width + xDelta, posY, posY + height)) {
                if (b == null) {
                    continue;
                }

                BoundingBox bbbBox = b.getBoundingBox();
                Point2D low = new Point2D(bbox.getMaxX() + xDelta, bbox.getMinY());
                Point2D mid = new Point2D(bbox.getMaxX() + xDelta, (bbox.getMinY() + bbox.getMaxY()) / 2);
                Point2D high = new Point2D(bbox.getMaxX() + xDelta, bbox.getMaxY());
                if (!b.isTrapasable() && (bbbBox.contains(low)
                        || bbbBox.contains(mid)
                        || bbbBox.contains(high))) {
                    this.speedX = 0;
                    xDelta = bbbBox.getMinX() - (posX + width);
                    break;
                }
            }

            if((getRelativeX() + width + xDelta) > (WIDTH / 2D)) {
                // Move background and objects
                world.updateOffset(xDelta);
            }

            return xDelta;
        }

        // Check for left collisions
        for (IBlockData b : world.getBlocksSideAt(posX + xDelta, posY, posY + height)) {
            if (b == null) {
                continue;
            }

            BoundingBox bbbBox = b.getBoundingBox();
            Point2D low = new Point2D(bbox.getMinX() + xDelta, bbox.getMinY());
            Point2D mid = new Point2D(bbox.getMinX() + xDelta, (bbox.getMinY() + bbox.getMaxY()) / 2);
            Point2D high = new Point2D(bbox.getMinX() + xDelta, bbox.getMaxY());
            if (!b.isTrapasable() && (bbbBox.contains(low)
                    || bbbBox.contains(mid)
                    || bbbBox.contains(high))) {
                this.speedX = 0;
                xDelta = bbbBox.getMaxX() - posX;
                break;
            }
        }

        // Se la distanza fra la posizione del player sullo schermo - delta x è minore di 50
        if((getRelativeX() + xDelta) < 50) {
            // Se la posizione assoluta del player - delta x è negativa
            if(posX + xDelta < 0) {
                // Si azzera l'accellerazione e si azzera la posizione del player.
                this.speedX = 0;
                xDelta = -posX;
            }

            // L'offset - delta x è positivo
            if(world.getOffset() + xDelta >= 0) {
                // Move background and objects
                world.updateOffset(xDelta);
            }
            // L'offset è positivo, ma minore del delta x -> viene quindi azzerato
            else if(world.getOffset() != 0) {
                // Move background and objects
                world.updateOffset(world.getOffset());
            }
        }

        return xDelta;
    }

    @Override
    public void setDead(boolean dead) {
        super.setDead(dead);

        Game.getInstance().decrementLives();
    }

    @Override
    public String getTexture() {
        return "entity.player.still";
    }

    @Override
    public String getTextureMove() {
        return "entity.player.move.1";
    }
}
