package org.valdi.cicciopier.entity;

import org.valdi.cicciopier.world.World;

import java.util.Optional;

import static org.valdi.cicciopier.GameDetails.getBlockSize;

public class Shuriken extends Enemy {
    private static final int SPEED = 7;

    public Shuriken(World world) {
        super(world);

        setMaxHealth(100D);
        setHealth(100D);

        setWidth(getBlockSize() / 4D);
        setHeight(getBlockSize() / 4D);

        setDamage(25D);
    }

    public void setDirection(boolean direction) {
        this.speedX = direction ? SPEED : -SPEED;
    }

    @Override
    public String getTexture() {
        return "entity.shuriken";
    }

    @Override
    public String getTextureMove() {
        return "entity.shuriken";
    }

    @Override
    protected void tickAI() {
        Optional<Player> player = world.getPlayer();
        if(!player.isPresent()) {
            return;
        }

        if(damagePlayer(player.get(), true)) {
            this.remove();
        }
    }

    @Override
    protected void tickMovement() {
        move(speedX, speedY);
    }

    @Override
    protected void moveImpl(double xDelta, double yDelta) {
        //MOVE_LOGGER.info("Player: {}", this.getBoundingBox());

        // Do collision detection here. Upon collision, set speedX/speedY to zero..!
        yDelta = checkVerticalCollisions(yDelta);
        xDelta = checkHorizontalCollisions(xDelta);
        if(xDelta == 0) {
            this.remove();
            return;
        }

        this.posX += xDelta;
        this.posY += yDelta;
    }
}
