package org.valdi.cicciopier.entity;

import com.google.gson.JsonObject;
import javafx.geometry.BoundingBox;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import org.valdi.cicciopier.GameDetails;
import org.valdi.cicciopier.assets.ITexture;
import org.valdi.cicciopier.key.GameKey;
import org.valdi.cicciopier.IRenderizable;
import org.valdi.cicciopier.ITickable;
import org.valdi.cicciopier.key.TexturesKey;
import org.valdi.cicciopier.registries.Registries;
import org.valdi.cicciopier.world.World;

import java.util.Optional;
import java.util.UUID;

public abstract class Entity implements ITickable, IRenderizable {
    protected volatile GameKey key;
    protected UUID uuid;
    protected World world;

    protected boolean toRemove;

    protected double posX;
    protected double posY;

    protected double speedX;
    protected double speedY;

    protected double width;
    protected double height;

    protected Entity(World world) {
        this.uuid = UUID.randomUUID();
        this.world = world;

        this.toRemove = false;

        this.posX = 0;
        this.posY = 0;

        this.speedX = 0;
        this.speedY = 0;

        this.width = 0;
        this.height = 0;
    }

    // TODO: reading & writing entity to json.

    @Override
    public void doTick() {

    }

    @Override
    public void doRender(GraphicsContext g) {
        Optional<ITexture> texture = Registries.TEXTURES.getOptional(new TexturesKey(getTexture()));
        texture.ifPresent(iTexture -> g.drawImage(iTexture.getTexture(), getRelativeX(), posY, width, height));

        showBounds(g);
    }

    protected void showBounds(GraphicsContext g) {
        if(GameDetails.SHOW_BOUNDS) {
            g.setStroke(Color.BLUE);
            g.strokeRect(getRelativeX(), posY, width, height);
        }
    }

    public GameKey getKey() {
        if(key == null) {
            this.key = Registries.ENTITIES.getKey(this.getClass());
        }
        return key;
    }

    public UUID getUuid() {
        return uuid;
    }

    public World getWorld() {
        return world;
    }

    public boolean isRemove() {
        return toRemove;
    }

    public void remove() {
        this.toRemove = true;
    }

    public double getX() {
        return posX;
    }

    public void setX(double posX) {
        this.posX = posX;
    }

    public double getY() {
        return posY;
    }

    public void setY(double posY) {
        this.posY = posY;
    }

    public double getRelativeX() {
        return posX - world.getOffset();
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public BoundingBox getBoundingBox() {
        return new BoundingBox(posX, posY, width - 1, height - 1);
    }

    public void fromLoad(JsonObject json) {

    }

    public String getTexture() {
        return null;
    }
}
