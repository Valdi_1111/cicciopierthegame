package org.valdi.cicciopier.entity;

import javafx.geometry.BoundingBox;
import javafx.geometry.Point2D;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.valdi.cicciopier.assets.ITexture;
import org.valdi.cicciopier.assets.Sounds;
import org.valdi.cicciopier.block.IBlockData;
import org.valdi.cicciopier.key.TexturesKey;
import org.valdi.cicciopier.registries.Registries;
import org.valdi.cicciopier.world.World;

import java.util.Optional;

import static org.valdi.cicciopier.GameDetails.*;

public abstract class LivingEntity extends Entity {
    protected static final Logger MOVE_LOGGER = LogManager.getLogger("Move");

    protected boolean prevDirection;
    protected double maxHealth;
    protected double health;
    protected boolean dead;
    protected boolean canJump;

    protected LivingEntity(World world) {
        super(world);

        this.prevDirection = true;
        this.maxHealth = 0;
        this.health = 0;
        this.dead = false;
        this.canJump = false;
    }

    public double getMaxHealth() {
        return maxHealth;
    }

    public void setMaxHealth(double maxHealth) {
        this.maxHealth = maxHealth;
    }

    public double getHealth() {
        return health;
    }

    public void setHealth(double health) {
        this.health = health;
    }

    public void damage(double damage, DamageCause cause) {
        this.health -= damage;
        if (health <= 0) {
            health = 0;
            this.setDead(true);
        }
    }

    public boolean isDead() {
        return dead;
    }

    public void setDead(boolean dead) {
        this.dead = dead;
    }

    @Override
    public void doRender(GraphicsContext g) {
        String t = speedX == 0 ? getTexture() : getTextureMove();
        Optional<ITexture> texture = Registries.TEXTURES.getOptional(new TexturesKey(t));

        if(texture.isPresent()) {
            Image img = texture.get().getTexture();
            if(speedX > 0) { // Right
                g.drawImage(img, getRelativeX(), posY, width, height);
            }
            else if(speedX < 0) { // Left
                g.drawImage(img, getRelativeX() + width, posY, -width, height);
            }
            else {
                if(prevDirection) { // Prev was Right
                    g.drawImage(img, getRelativeX(), posY, width, height);
                }
                else { // Prev was Left
                    g.drawImage(img, getRelativeX() + width, posY, -width, height);
                }
            }
        }

        showBounds(g);
    }

    @Override
    public void doTick() {
        super.doTick();

        // Se sei nel void:
        if (posY > getBlockSize() * 25) {
            this.damage(50D, DamageCause.VOID);
        }

        tickMovement();
    }

    protected void tickMovement() {
        this.speedX *= FRICTION;
        this.speedY *= FRICTION;

        move(speedX, speedY);
        if(speedX != 0) {
            this.prevDirection = speedX > 0;
        }

        // Gravity accelerates the object downwards each tick
        accelerate(0, GRAVITY);
    }

    public void runRight() {
        accelerate(SPEED, 0);
    }

    public void runLeft() {
        accelerate(-SPEED, 0);
    }

    public void jump() {
        if (!canJump) {
            return;
        }

        //Sounds.PLAYER_JUMP.getSound().play();
        //MediaPlayer music = new MediaPlayer(Sounds.PLAYER_JUMP.getSound());
        //music.play();

        accelerate(0, -JUMP_STRENGTH);
        this.canJump = false;
    }

    public void accelerate(double accelerationX, double accelerationY) {
        this.speedX += accelerationX;
        this.speedY += accelerationY;
    }

    public void move(double xDelta, double yDelta) {
        xDelta = Math.floor(xDelta);
        yDelta = Math.floor(yDelta);

        //MOVE_LOGGER.info("xDelta: {}, yDelta: {}, player: {}", xDelta, yDelta, this.getBoundingBox());

        /*
        // Do collision detection here. Upon collision, set speedX/speedY to zero..!
        yDelta = checkVerticalCollisions(yDelta);
        xDelta = checkHorizontalCollisions(xDelta);

        this.posX += xDelta;
        this.posY += yDelta;
        */

        double times = Math.max(Math.abs(xDelta), Math.abs(yDelta));
        times = Math.ceil(times);
        //MOVE_LOGGER.info("xDelta: {}, yDelta: {}, times: {}", xDelta, yDelta, times);
        for(int i = 1; i <= times; i++) {
            //MOVE_LOGGER.info("ciclying time: {}, xDelta: {}, yDelta: {}", i, xDelta, yDelta);
            double moveX = 0D;
            if(xDelta >= 1) {
                moveX = 1;
                xDelta--;
            } else if(xDelta > 0) {
                //moveX = xDelta;
                xDelta = 0;
            }

            if(xDelta <= -1) {
                moveX = -1;
                xDelta++;
            } else if(xDelta < 0) {
                //moveX = xDelta;
                xDelta = 0;
            }

            double moveY = 0D;
            if(yDelta >= 1) {
                moveY = 1;
                yDelta--;
            } else if(yDelta > 0) {
                //moveY = yDelta;
                yDelta = 0;
            }

            if(yDelta <= -1) {
                moveY = -1;
                yDelta++;
            } else if(yDelta < 0) {
                //moveY = yDelta;
                yDelta = 0;
            }

            moveImpl(moveX, moveY);
        }
    }

    protected void moveImpl(double xDelta, double yDelta) {
        //MOVE_LOGGER.info("Player: {}", this.getBoundingBox());

        // Do collision detection here. Upon collision, set speedX/speedY to zero..!
        yDelta = checkVerticalCollisions(yDelta);
        xDelta = checkHorizontalCollisions(xDelta);

        this.posX += xDelta;
        this.posY += yDelta;
    }

    protected double checkHorizontalCollisions(double xDelta) {
        if(xDelta == 0) {
            return 0;
        }

        BoundingBox bbox = this.getBoundingBox();
        if (xDelta > 0) {
            // Check for right collisions
            for (IBlockData b : world.getBlocksSideAt(posX + width + xDelta, posY, posY + height)) {
                if (b == null) {
                    continue;
                }

                BoundingBox bbbBox = b.getBoundingBox();
                Point2D low = new Point2D(bbox.getMaxX() + xDelta, bbox.getMinY());
                Point2D mid = new Point2D(bbox.getMaxX() + xDelta, (bbox.getMinY() + bbox.getMaxY()) / 2);
                Point2D high = new Point2D(bbox.getMaxX() + xDelta, bbox.getMaxY());
                if (!b.isTrapasable() && (bbbBox.contains(low)
                        || bbbBox.contains(mid)
                        || bbbBox.contains(high))) {
                    this.speedX = 0;
                    xDelta = bbbBox.getMinX() - (posX + width);
                    break;
                }
            }
            return xDelta;
        }

        // Check for left collisions
        for (IBlockData b : world.getBlocksSideAt(posX + xDelta, posY, posY + height)) {
            if (b == null) {
                continue;
            }

            BoundingBox bbbBox = b.getBoundingBox();
            Point2D low = new Point2D(bbox.getMinX() + xDelta, bbox.getMinY());
            Point2D mid = new Point2D(bbox.getMinX() + xDelta, (bbox.getMinY() + bbox.getMaxY()) / 2);
            Point2D high = new Point2D(bbox.getMinX() + xDelta, bbox.getMaxY());
            if (!b.isTrapasable() && (bbbBox.contains(low)
                    || bbbBox.contains(mid)
                    || bbbBox.contains(high))) {
                this.speedX = 0;
                xDelta = bbbBox.getMaxX() - posX;
                break;
            }
        }
        return xDelta;
    }

    protected double checkVerticalCollisions(double yDelta) {
        if(yDelta == 0) {
            return 0;
        }

        BoundingBox bbox = this.getBoundingBox();
        if (yDelta > 0) {
            // Check for down collisions
            for (IBlockData b : world.getBlocksDownAt(posX, posY + height + yDelta)) {
                if (b == null) {
                    continue;
                }

                BoundingBox bbbBox = b.getBoundingBox();
                Point2D low = new Point2D(bbox.getMinX(), bbox.getMaxY() + yDelta);
                Point2D high = new Point2D(bbox.getMaxX(), bbox.getMaxY() + yDelta);
                if (!b.isTrapasable() && (bbbBox.contains(low)
                        || bbbBox.contains(high))) {
                    this.speedY = 0;
                    yDelta = bbbBox.getMinY() - (posY + height);
                    this.canJump = true;
                    break;
                }
            }
            return yDelta;
        }

        if(posY + yDelta < 0) {
            this.speedY = 0;
            yDelta = 0 - posY;
        }
        // Check for up collisions
        for (IBlockData b : world.getBlocksDownAt(posX, posY + yDelta)) {
            if (b == null) {
                continue;
            }

            BoundingBox bbbBox = b.getBoundingBox();
            Point2D low = new Point2D(bbox.getMinX(), bbox.getMinY() + yDelta);
            Point2D high = new Point2D(bbox.getMaxX(), bbox.getMinY() + yDelta);
            if (!b.isTrapasable() && (bbbBox.contains(low)
                    || bbbBox.contains(high))) {
                this.speedY = 0;
                yDelta = bbbBox.getMaxY() - posY;
                break;
            }
        }
        return yDelta;
    }

    public String getTexture() {
        return null;
    }

    public String getTextureMove() {
        return null;
    }

}
