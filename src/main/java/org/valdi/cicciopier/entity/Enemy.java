package org.valdi.cicciopier.entity;

import org.valdi.cicciopier.world.World;

public class Enemy extends LivingEntity {
    protected double damage;
    protected long lastDamage;

    protected Enemy(World world) {
        super(world);

        this.damage = 0D;
        this.lastDamage = System.currentTimeMillis();
    }

    @Override
    public void doTick() {
        super.doTick();

        tickAI();
    }

    protected void tickAI() {

    }

    protected boolean damagePlayer(Player player, boolean ignore) {
        if (!player.getBoundingBox().intersects(this.getBoundingBox())) {
            return false;
        }

        long now = System.currentTimeMillis();
        if (!ignore && now - lastDamage < 1000L) {
            return false;
        }

        this.lastDamage = now;
        player.damage(getDamage(), DamageCause.ENEMY);
        return true;
    }

    public double getDamage() {
        return damage;
    }

    public void setDamage(double damage) {
        this.damage = damage;
    }
}
