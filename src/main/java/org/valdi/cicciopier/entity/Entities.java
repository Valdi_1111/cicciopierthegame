package org.valdi.cicciopier.entity;

import org.valdi.cicciopier.key.EntitiesKey;
import org.valdi.cicciopier.key.GameKey;
import org.valdi.cicciopier.registries.AutoRegister;

public class Entities implements AutoRegister {
    public static final EntityEntry PLAYER = new EntityEntry(new EntitiesKey("player"), Player.class);
    public static final EntityEntry ANANAS = new EntityEntry(new EntitiesKey("ananas"), Ananas.class);
    public static final EntityEntry CIPALLOTTOLA = new EntityEntry(new EntitiesKey("cipallottola"), Cipallottola.class);
    public static final EntityEntry FINOCCHIO_NINJA = new EntityEntry(new EntitiesKey("finocchio_ninja"), FinocchioNinja.class);
    public static final EntityEntry SHURIKEN = new EntityEntry(new EntitiesKey("shuriken"), Shuriken.class);

    public static class EntityEntry {
        private final Class<? extends Entity> clazz;
        private final GameKey key;

        public EntityEntry(GameKey key, Class<? extends Entity> clazz) {
            this.clazz = clazz;
            this.key = key;
        }

        public Class<? extends Entity> getClazz() {
            return clazz;
        }

        public GameKey getKey() {
            return key;
        }
    }

}
