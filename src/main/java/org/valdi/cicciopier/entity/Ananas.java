package org.valdi.cicciopier.entity;

import org.valdi.cicciopier.world.World;

import java.util.Optional;

import static org.valdi.cicciopier.GameDetails.getBlockSize;

public class Ananas extends Enemy {

    public Ananas(World world) {
        super(world);

        setMaxHealth(100D);
        setHealth(100D);

        setWidth(getBlockSize());
        setHeight(getBlockSize() * 2 - getBlockSize() / 16D);

        setDamage(50D);
    }

    @Override
    protected void tickAI() {
        Optional<Player> player = world.getPlayer();
        if (!player.isPresent()) {
            return;
        }

        double distance = (player.get().getX() + player.get().getWidth() / 2) - (posX + width / 2);
        if (distance > 10) {
            prevDirection = true;
        } else if (distance < -10) {
            prevDirection = false;
        }

        damagePlayer(player.get(), false);
    }

    @Override
    public String getTexture() {
        return "entity.ananas.still";
    }

    @Override
    public String getTextureMove() {
        return "entity.ananas.move";
    }
}
