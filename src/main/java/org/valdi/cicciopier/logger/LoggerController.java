package org.valdi.cicciopier.logger;

import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import org.valdi.cicciopier.Main;
import org.valdi.cicciopier.TextAreaAppender;

public class LoggerController {
    private final Main main;

    @FXML
    private TextArea loggerArea;

    public LoggerController(Main main) {
        this.main = main;
    }

    @FXML
    private void initialize() {
        //TextAreaAppender.addTextArea(loggerArea);
    }
}
