package org.valdi.cicciopier.window;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import org.valdi.cicciopier.Game;
import org.valdi.cicciopier.GameState;
import org.valdi.cicciopier.Main;

public class GameWindowController {
    private final Main main;

    @FXML
    private AnchorPane pane;
    @FXML
    private GameWindow window;
    @FXML
    private Button restart;

    public GameWindowController(Main main) {
        this.main = main;
    }

    @FXML
    private void onRestart() {
        Game game = main.getGame();
        if(game.getState() == GameState.DEAD) {
            game.setState(GameState.WAITING_RESET);
        }
        if(game.getState() == GameState.FULL_DEAD) {
            game.setState(GameState.WAITING_FULL_RESET);
        }
        if(game.getState() == GameState.WON) {
            game.setState(GameState.WAITING_FULL_RESET);
        }

        restart.setVisible(false);
    }

}
