package org.valdi.cicciopier.window;

import javafx.scene.canvas.Canvas;

public class GameWindow extends Canvas {

    @Override
    public boolean isResizable() {
        return true;
    }

    @Override
    public void resize(double width, double height) {
        super.resize(width, height);

        setWidth(width);
        setHeight(height);
    }
}
