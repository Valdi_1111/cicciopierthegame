package org.valdi.cicciopier.block;

import javafx.geometry.BoundingBox;
import javafx.scene.canvas.GraphicsContext;
import org.valdi.cicciopier.IRenderizable;
import org.valdi.cicciopier.ITickable;
import org.valdi.cicciopier.world.World;

import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.valdi.cicciopier.GameDetails.getBlockSize;

public class IBlockData implements IBlockDataHolder<IBlockData>, ITickable, IRenderizable {
    public static final Function<Map.Entry<IBlockState<?>, Comparable<?>>, String> STATE_TO_VALUE = new Function<Map.Entry<IBlockState<?>, Comparable<?>>, String>(){

        @Override
        public String apply(Map.Entry<IBlockState<?>, Comparable<?>> entry) {
            if (entry == null) {
                return "<NULL>";
            }
            IBlockState<?> state = entry.getKey();
            return state.getName() + "=" + this.serialize(state, entry.getValue());
        }

        private <T extends Comparable<T>> String serialize(IBlockState<T> state, Comparable<?> value) {
            return state.serialize((T) value);
        }
    };

    private World world;

    private int posX;
    private int posY;

    private final Block block;
    private final Map<IBlockState<?>, Comparable<?>> states;

    private boolean remove;

    public IBlockData(World world, Block block, Map<IBlockState<?>, Comparable<?>> states) {
        this.world = world;

        this.posX = 0;
        this.posY = 0;

        this.block = block;
        this.states = states;

        this.remove = false;
    }

    public Block getBlock() {
        return block;
    }

    @Override
    public <T extends Comparable<T>> T get(IBlockState<T> state) {
        Comparable<?> value = states.get(state);
        if (value == null) {
            throw new IllegalArgumentException("Cannot get property " + state + " as it does not exist in " + block);
        }
        return state.getClazz().cast(value);
    }

    @Override
    public <T extends Comparable<T>, V extends T> IBlockData set(IBlockState<T> state, V value) {
        if (!states.containsKey(state)) {
            throw new IllegalArgumentException("Cannot set property " + state + " as it does not exist in " + block);
        }
        Comparable<?> currentValue = states.get(state);
        if (currentValue == value) {
            return this;
        }
        states.put(state, value);
        return this;
    }

    @Override
    public Map<IBlockState<?>, Comparable<?>> getStateMap() {
        return states;
    }

    public boolean isTrapasable() {
        return block.isTrapasable(this);
    }

    public boolean isAir() {
        return block.isAir();
    }

    @Override
    public void doRender(GraphicsContext g) {
        block.doRender(g, this);
    }

    @Override
    public void doTick() {
        block.doTick(this);
    }

    public BoundingBox getBoundingBox() {
        return new BoundingBox(getX(), getY(), getBlockSize(), getBlockSize());
    }

    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(block);
        if (!this.getStateMap().isEmpty()) {
            builder.append('[');
            builder.append(this.getStateMap().entrySet().stream().map(STATE_TO_VALUE).collect(Collectors.joining(",")));
            builder.append(']');
        }
        return builder.toString();
    }

    public int getBlockX() {
        return posX;
    }

    public void setBlockX(int posX) {
        this.posX = posX;
    }

    public int getBlockY() {
        return posY;
    }

    public void setBlockY(int posY) {
        this.posY = posY;
    }

    public int getX() {
        return posX * getBlockSize();
    }

    public double getRelativeX() {
        return getX() - world.getOffset();
    }

    public int getY() {
        return posY * getBlockSize();
    }

    public World getWorld() {
        return world;
    }

    public boolean isRemove() {
        return remove;
    }

    public void remove() {
        this.remove = true;
    }
}
