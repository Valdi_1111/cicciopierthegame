package org.valdi.cicciopier.block;

import java.util.Map;

public interface IBlockDataHolder<C> {

    <T extends Comparable<T>> T get(IBlockState<T> state);

    <T extends Comparable<T>, V extends T> C set(IBlockState<T> state, V value);

    Map<IBlockState<?>, Comparable<?>> getStateMap();

}
