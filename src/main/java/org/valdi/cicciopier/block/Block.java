package org.valdi.cicciopier.block;

import com.google.gson.JsonObject;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import org.valdi.cicciopier.assets.ITexture;
import org.valdi.cicciopier.key.GameKey;
import org.valdi.cicciopier.key.TexturesKey;
import org.valdi.cicciopier.registries.Registries;
import org.valdi.cicciopier.registries.RegistryElement;
import org.valdi.cicciopier.world.World;

import java.util.HashMap;
import java.util.Optional;

import static org.valdi.cicciopier.GameDetails.WIDTH;
import static org.valdi.cicciopier.GameDetails.getBlockSize;

public class Block implements RegistryElement {
    protected final GameKey key;
    protected boolean trapasable;

    public Block(GameKey key) {
        this.key = key;
        this.trapasable = false;
    }

    @Override
    public GameKey getKey() {
        return key;
    }

    public IBlockData createBlockData(World world) {
        return new IBlockData(world, this, new HashMap<>());
    }

    @Override
    public String toString() {
        return "Block{" +
                "key=" + key +
                ", trapasable=" + trapasable +
                '}';
    }

    public void doTick(IBlockData blockData) {

    }

    public void doRender(GraphicsContext g, IBlockData blockData) {
        if(isAir()) {
            return;
        }

        double size = getBlockSize();
        double x = blockData.getRelativeX();
        double y = blockData.getY();

        if(x + size < 0 || x > WIDTH) {
            return;
        }

        if(renderTexture(g, x, y, size)) {
            return;
        }

        double halfSize = size / 2;

        g.setFill(Color.PURPLE);
        g.fillRect(x, y, halfSize, halfSize);
        g.fillRect(x + halfSize, y + halfSize, halfSize, halfSize);

        g.setFill(Color.BLACK);
        g.fillRect(x + halfSize, y, halfSize, halfSize);
        g.fillRect(x, y + halfSize, halfSize, halfSize);
    }

    protected boolean renderTexture(GraphicsContext g, double x, double y, double size) {
        String t = getTexture();
        Optional<ITexture> loc = Registries.TEXTURES.getOptional(new TexturesKey(t));
        if(!loc.isPresent()) {
            return false;
        }

        g.drawImage(loc.get().getTexture(), x, y, size, size);
        return true;
    }

    public void fromLoad(IBlockData blockData, JsonObject json) {

    }

    public boolean isTrapasable(IBlockData blockData) {
        return trapasable;
    }

    public void setTrapasable(boolean trapasable) {
        this.trapasable = trapasable;
    }

    public boolean isAir() {
        return false;
    }

    public String getTexture() {
        return null;
    }
}
