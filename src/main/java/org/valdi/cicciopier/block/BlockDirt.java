package org.valdi.cicciopier.block;

import org.valdi.cicciopier.key.BlocksKey;

public class BlockDirt extends Block {

    public BlockDirt() {
        super(new BlocksKey("dirt"));

        setTrapasable(false);
    }

    @Override
    public String  getTexture() {
        return "block.dirt";
    }

}
