package org.valdi.cicciopier.block;

import org.valdi.cicciopier.key.BlocksKey;

public class BlockGround extends Block {

    public BlockGround() {
        super(new BlocksKey("ground"));

        setTrapasable(false);
    }

    @Override
    public String  getTexture() {
        return "block.ground";
    }

}
