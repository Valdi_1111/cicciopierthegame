package org.valdi.cicciopier.block;

import javafx.scene.canvas.GraphicsContext;
import org.valdi.cicciopier.key.BlocksKey;

public class BlockAir extends Block {

    public BlockAir() {
        super(new BlocksKey("air"));

        setTrapasable(true);
    }

    @Override
    public boolean isAir() {
        return true;
    }

    @Override
    public void doRender(GraphicsContext g, IBlockData blockData) {
        //
    }
}
