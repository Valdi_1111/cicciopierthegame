package org.valdi.cicciopier.block;

import org.valdi.cicciopier.key.BlocksKey;

public class BlockWall extends Block {

    public BlockWall() {
        super(new BlocksKey("wall"));

        setTrapasable(false);
    }

    @Override
    public String  getTexture() {
        return "block.wall";
    }

}
