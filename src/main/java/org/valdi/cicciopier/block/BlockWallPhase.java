package org.valdi.cicciopier.block;

import com.google.gson.JsonObject;
import javafx.scene.canvas.GraphicsContext;
import org.valdi.cicciopier.key.BlocksKey;
import org.valdi.cicciopier.world.World;

import java.util.HashMap;
import java.util.Map;

public class BlockWallPhase extends Block {
    public static final BlockStateBoolean PHASE = BlockProperties.PHASE;

    public BlockWallPhase() {
        super(new BlocksKey("wall_phase"));

        setTrapasable(false);
    }

    @Override
    public IBlockData createBlockData(World world) {
        Map<IBlockState<?>, Comparable<?>> states = new HashMap<>();
        states.put(PHASE, false);

        return new IBlockData(world, this, states);
    }

    @Override
    public void fromLoad(IBlockData blockData, JsonObject json) {
        if(json.has("phase")) {
            blockData.set(PHASE, json.get("phase").getAsBoolean());
        }
    }

    @Override
    public void doRender(GraphicsContext g, IBlockData blockData) {
        if(!blockData.get(PHASE)) {
            return;
        }

        super.doRender(g, blockData);
    }

    @Override
    public String  getTexture() {
        return "block.wall";
    }

    @Override
    public boolean isTrapasable(IBlockData blockData) {
        if(!blockData.get(PHASE)) {
            return true;
        }

        return super.isTrapasable(blockData);
    }
}
