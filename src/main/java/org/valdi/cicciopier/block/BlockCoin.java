package org.valdi.cicciopier.block;

import com.google.gson.JsonObject;
import org.valdi.cicciopier.Game;
import org.valdi.cicciopier.entity.Player;
import org.valdi.cicciopier.key.BlocksKey;
import org.valdi.cicciopier.world.World;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class BlockCoin extends Block {
    public static final BlockStateInteger VALUE = BlockProperties.VALUE;

    public BlockCoin() {
        super(new BlocksKey("coin"));

        setTrapasable(true);
    }

    @Override
    public IBlockData createBlockData(World world) {
        Map<IBlockState<?>, Comparable<?>> states = new HashMap<>();
        states.put(VALUE, 0);

        return new IBlockData(world, this, states);
    }

    @Override
    public void doTick(IBlockData blockData) {
        Optional<Player> player = blockData.getWorld().getPlayer();
        if(!player.isPresent()) {
            return;
        }

        if(player.get().getBoundingBox().intersects(blockData.getBoundingBox())) {
            Game.getInstance().incrementCoins(blockData.get(VALUE));
            blockData.remove();
        }
    }

    @Override
    public void fromLoad(IBlockData blockData, JsonObject json) {
        if(json.has("value")) {
            blockData.set(VALUE, json.get("value").getAsInt());
        }
    }

    @Override
    public String  getTexture() {
        return "block.coin";
    }
}
