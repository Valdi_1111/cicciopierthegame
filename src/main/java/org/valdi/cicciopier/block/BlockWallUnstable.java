package org.valdi.cicciopier.block;

import javafx.geometry.BoundingBox;
import javafx.geometry.Point2D;
import org.valdi.cicciopier.Game;
import org.valdi.cicciopier.entity.Player;
import org.valdi.cicciopier.key.BlocksKey;
import org.valdi.cicciopier.world.World;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

public class BlockWallUnstable extends Block {
    public static final BlockStateBoolean FALLING = BlockProperties.FALLING;

    public BlockWallUnstable() {
        super(new BlocksKey("wall_unstable"));

        setTrapasable(false);
    }

    @Override
    public IBlockData createBlockData(World world) {
        Map<IBlockState<?>, Comparable<?>> states = new HashMap<>();
        states.put(FALLING, false);

        return new IBlockData(world, this, states);
    }

    @Override
    public String  getTexture() {
        return "block.wall_unstable";
    }

    @Override
    public void doTick(IBlockData blockData) {
        if(blockData.get(FALLING)) {
            //blockData.setBlockX(blockData.getX() + 1);
            //blockData.remove();
            return;
        }

        Optional<Player> player = blockData.getWorld().getPlayer();
        if(!player.isPresent()) {
            return;
        }

        BoundingBox bbox = player.get().getBoundingBox();
        Point2D min = new Point2D(bbox.getMinX(), bbox.getMaxY() + 2);
        Point2D max = new Point2D(bbox.getMaxX(), bbox.getMaxY() + 2);

        BoundingBox bbbox = blockData.getBoundingBox();
        if(bbbox.contains(max) || bbbox.contains(min)) {
            blockData.set(FALLING, true);
            Game.getInstance().getScheduler().runTaskLater(blockData::remove, 1L, TimeUnit.SECONDS);
        }
    }

    @Override
    public boolean isTrapasable(IBlockData blockData) {
        /*if(blockData.get(FALLING)) {
            return true;
        }*/

        return super.isTrapasable(blockData);
    }
}
