package org.valdi.cicciopier.block;

import org.valdi.cicciopier.key.BlocksKey;

public class BlockGrass extends Block {

    public BlockGrass() {
        super(new BlocksKey("grass"));

        setTrapasable(false);
    }

    @Override
    public String  getTexture() {
        return "block.grass";
    }

}
