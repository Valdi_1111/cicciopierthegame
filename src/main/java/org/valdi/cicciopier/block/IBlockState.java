package org.valdi.cicciopier.block;

import java.util.Collection;
import java.util.Optional;

public interface IBlockState<T extends Comparable<T>> {

    String getName();

    Collection<T> getValues();

    Class<T> getClazz();

    Optional<T> deserialize(String var1);

    String serialize(T var1);

}
