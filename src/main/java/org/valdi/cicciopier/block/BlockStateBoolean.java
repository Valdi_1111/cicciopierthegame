package org.valdi.cicciopier.block;

import com.google.common.collect.ImmutableSet;

import java.util.Collection;
import java.util.Optional;

public class BlockStateBoolean extends BlockState<Boolean> {
    private final ImmutableSet<Boolean> values = ImmutableSet.of(true, false);

    public BlockStateBoolean(String name) {
        super(name, Boolean.class);
    }

    @Override
    public Collection<Boolean> getValues() {
        return this.values;
    }

    public static BlockStateBoolean of(String name) {
        return new BlockStateBoolean(name);
    }

    @Override
    public Optional<Boolean> deserialize(String value) {
        if ("true".equals(value) || "false".equals(value)) {
            return Optional.of(Boolean.valueOf(value));
        }
        return Optional.empty();
    }

    @Override
    public String serialize(Boolean value) {
        return value.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BlockStateBoolean)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        BlockStateBoolean that = (BlockStateBoolean) o;
        return values.equals(that.values);
    }

    @Override
    public int hashCode() {
        return 31 * super.hashCode() + values.hashCode();
    }
}
