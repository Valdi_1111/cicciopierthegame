package org.valdi.cicciopier.block;

import javafx.scene.canvas.GraphicsContext;
import org.valdi.cicciopier.assets.ITexture;
import org.valdi.cicciopier.entity.Player;
import org.valdi.cicciopier.key.BlocksKey;
import org.valdi.cicciopier.key.TexturesKey;
import org.valdi.cicciopier.registries.Registries;
import org.valdi.cicciopier.world.World;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class BlockButton extends Block {
    public static final BlockStateBoolean POWERED = BlockProperties.POWERED;
    public static boolean ACTIVE = false;

    private long lastUsage;

    public BlockButton() {
        super(new BlocksKey("button"));

        setTrapasable(true);
        this.lastUsage = System.currentTimeMillis();
    }

    @Override
    public IBlockData createBlockData(World world) {
        Map<IBlockState<?>, Comparable<?>> states = new HashMap<>();
        states.put(POWERED, false);

        return new IBlockData(world, this, states);
    }

    @Override
    public void doTick(IBlockData blockData) {
        Optional<Player> player = blockData.getWorld().getPlayer();
        if(!player.isPresent()) {
            return;
        }

        if(!player.get().getBoundingBox().intersects(blockData.getBoundingBox())) {
            return;
        }

        long now = System.currentTimeMillis();
        if(now - lastUsage < 1000L) {
            return;
        }

        this.lastUsage = now;
        ACTIVE = !ACTIVE;
        blockData.getWorld().getBlocks().values().stream().filter(d -> d.getBlock() == Blocks.WALL_PHASE).forEach(d -> {
            boolean prevValue = d.get(BlockWallPhase.PHASE);
            d.set(BlockWallPhase.PHASE, !prevValue);
        });
    }

    @Override
    protected boolean renderTexture(GraphicsContext g, double x, double y, double size) {
        String t = getTexture();
        Optional<ITexture> loc = Registries.TEXTURES.getOptional(new TexturesKey(t));
        if(!loc.isPresent()) {
            return false;
        }

        double ySize = ACTIVE ? 8 : 16;
        g.drawImage(loc.get().getTexture(), x, y + size - ySize, size, ySize);
        return true;
    }

    @Override
    public String getTexture() {
        return ACTIVE ? "block.button_pressed" : "block.button";
    }
}
