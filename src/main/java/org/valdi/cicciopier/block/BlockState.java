package org.valdi.cicciopier.block;

public abstract class BlockState<T extends Comparable<T>> implements IBlockState<T> {
    private final Class<T> clazz;
    private final String name;

    protected BlockState(String name, Class<T> clazz) {
        this.clazz = clazz;
        this.name = name;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public Class<T> getClazz() {
        return this.clazz;
    }

    @Override
    public String toString() {
        return "BlockState{" +
                "clazz=" + clazz +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BlockState)) {
            return false;
        }
        BlockState that = (BlockState) o;
        return clazz.equals(that.clazz) && name.equals(that.name);
    }

    @Override
    public int hashCode() {
        return 31 * name.hashCode() + clazz.hashCode();
    }
}
