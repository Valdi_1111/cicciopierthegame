package org.valdi.cicciopier.block;

import com.google.common.collect.ImmutableSet;
import org.apache.commons.lang3.StringUtils;

import java.util.Collection;
import java.util.Optional;

public class BlockStateInteger extends BlockState<Integer> {
    private final ImmutableSet<Integer> values = ImmutableSet.of(1, 2, 3, 4, 5);

    public BlockStateInteger(String name) {
        super(name, Integer.class);
    }

    @Override
    public Collection<Integer> getValues() {
        return this.values;
    }

    public static BlockStateInteger of(String name) {
        return new BlockStateInteger(name);
    }

    @Override
    public Optional<Integer> deserialize(String value) {
        if (StringUtils.isNumeric(value)) {
            return Optional.of(Integer.valueOf(value));
        }
        return Optional.empty();
    }

    @Override
    public String serialize(Integer value) {
        return value.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BlockStateInteger)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        BlockStateInteger that = (BlockStateInteger) o;
        return values.equals(that.values);
    }

    @Override
    public int hashCode() {
        return 31 * super.hashCode() + values.hashCode();
    }
}
