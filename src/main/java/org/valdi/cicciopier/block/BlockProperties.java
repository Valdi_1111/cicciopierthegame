package org.valdi.cicciopier.block;

public class BlockProperties {
    public static final BlockStateBoolean POWERED = BlockStateBoolean.of("powered");
    public static final BlockStateBoolean PHASE = BlockStateBoolean.of("phase");
    public static final BlockStateInteger VALUE = BlockStateInteger.of("value");
    public static final BlockStateBoolean FALLING = BlockStateBoolean.of("falling");
}
