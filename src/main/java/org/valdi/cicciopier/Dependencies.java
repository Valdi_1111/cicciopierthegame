package org.valdi.cicciopier;

import org.valdi.SuperApiX.common.dependencies.Dependency;
import org.valdi.SuperApiX.common.dependencies.IDependencyManager;

public class Dependencies {

    public static void loadAll(IDependencyManager dm) {
        dm.loadDependencies(
                Dependencies.GOOGLE_GUAVA,
                Dependencies.GOOGLE_GUAVA_FAILUREACCESS,
                Dependencies.GOOGLE_GSON,
                Dependencies.COMMONS_LANG3,
                Dependencies.JAR_RELOCATOR,
                Dependencies.LOG4J_SLF4J_BINDING);
        dm.loadDependencies(
                Dependencies.YAML,
                Dependencies.HOCON_CONFIG,
                Dependencies.TOML4J,
                Dependencies.CONFIGURATE_CORE,
                Dependencies.CONFIGURATE_YAML,
                Dependencies.CONFIGURATE_GSON,
                Dependencies.CONFIGURATE_HOCON,
                Dependencies.CONFIGURATE_TOML);
        dm.loadDependencies(
                Dependencies.MONGODB_DRIVER,
                Dependencies.MARIADB_DRIVER,
                Dependencies.MYSQL_DRIVER,
                Dependencies.POSTGRESQL_DRIVER,
                Dependencies.SQLITE_DRIVER,
                Dependencies.H2_DRIVER,
                Dependencies.HIKARI);
    }

    public static final Dependency JAR_RELOCATOR = Dependency
            .builder("JAR_RELOCATOR")
            .setGroupId("me.lucko")
            .setArtifactId("jar-relocator")
            .setVersion("1.3")
            .setChecksum("mmz3ltQbS8xXGA2scM0ZH6raISlt4nukjCiU2l9Jxfs=")
            .setAutoLoad(false)
            .build();

    public static final Dependency GOOGLE_GSON = Dependency
            .builder("GOOGLE_GSON")
            .setGroupId("com{}google{}code{}gson")
            .setArtifactId("gson")
            .setVersion("2.8.6")
            .setChecksum("yPtIOQVNKAswM/gA0fWpfeLwKOuLoutFitKH5Tbz8l8=")
            .setAutoLoad(true)
            .build();

    public static final Dependency GOOGLE_GUAVA = Dependency
            .builder("GOOGLE_GUAVA")
            .setGroupId("com{}google{}guava")
            .setArtifactId("guava")
            .setVersion("28.1-jre")
            .setChecksum("ML64uFJ70HxudH538akhIsLynVfONHRhpKVesm44LaQ=")
            .setAutoLoad(true)
            .build();

    public static final Dependency GOOGLE_GUAVA_FAILUREACCESS = Dependency
            .builder("GOOGLE_GUAVA_FAILUREACCESS")
            .setGroupId("com{}google{}guava")
            .setArtifactId("failureaccess")
            .setVersion("1.0.1")
            .setChecksum("oXHuTHNN0tqDfksWvp30Zhr6typBra8x64Tf2vk2yiY=")
            .setAutoLoad(true)
            .build();

    public static final Dependency COMMONS_LANG3 = Dependency
            .builder("COMMONS_LANG3")
            .setGroupId("org{}apache{}commons")
            .setArtifactId("commons-lang3")
            .setVersion("3.9")
            .setChecksum("3i4dzc8++ReozoWGYaBnJqmpRPKOM61/ngi+pE3DwjA=")
            .setAutoLoad(true)
            .build();

    public static final Dependency LOG4J_SLF4J_BINDING = Dependency
            .builder("LOG4J_SLF4J_BINDING")
            .setGroupId("org{}apache{}logging{}log4j")
            .setArtifactId("log4j-slf4j-impl")
            .setVersion("2.12.1")
            .setChecksum("PZYgr8PNWFJ6GCtw58ERtyiQRpicDQSlDkaw7DHcE4o=")
            .setAutoLoad(true)
            .build();

    public static final Dependency CONFIGURATE_CORE = Dependency
            .builder("CONFIGURATE_CORE")
            .setGroupId("me{}lucko{}configurate")
            .setArtifactId("configurate-core")
            .setVersion("3.5")
            .setChecksum("J+1WnX1g5gr4ne8qA7DuBadLDOsZnOZjwHbdRmVgF6c=")
            .setAutoLoad(true)
            .build();

    public static final Dependency YAML = Dependency
            .builder("YAML")
            .setGroupId("org{}yaml")
            .setArtifactId("snakeyaml")
            .setVersion("1.25")
            .setChecksum("tQ7zMYfn3JIrJtvk3Q/bOpzzSedaCLlSaZAVSO7lRus=")
            .setAutoLoad(true)
            .build();

    public static final Dependency CONFIGURATE_YAML = Dependency
            .builder("CONFIGURATE_YAML")
            .setGroupId("me{}lucko{}configurate")
            .setArtifactId("configurate-yaml")
            .setVersion("3.5")
            .setChecksum("Dxr1o3EPbpOOmwraqu+cors8O/nKwJnhS5EiPkTb3fc=")
            .setAutoLoad(true)
            .build();

    public static final Dependency CONFIGURATE_GSON = Dependency
            .builder("CONFIGURATE_GSON")
            .setGroupId("me{}lucko{}configurate")
            .setArtifactId("configurate-gson")
            .setVersion("3.5")
            .setChecksum("Q3wp3xpqy41bJW3yUhbHOzm+NUkT4bUUBI2/AQLaa3c=")
            .setAutoLoad(true)
            .build();

    public static final Dependency HOCON_CONFIG = Dependency
            .builder("HOCON_CONFIG")
            .setGroupId("com{}typesafe")
            .setArtifactId("config")
            .setVersion("1.3.3")
            .setChecksum("tfHWBx8VSNBb6C9Z+QOcfTeheHvY48Z34x7ida9KRiE=")
            .setAutoLoad(true)
            .build();

    public static final Dependency CONFIGURATE_HOCON = Dependency
            .builder("CONFIGURATE_HOCON")
            .setGroupId("me{}lucko{}configurate")
            .setArtifactId("configurate-hocon")
            .setVersion("3.5")
            .setChecksum("sOym1KPmQylGSfk90ZFqobuvoZfEWb7XMmMBwbHuxFw=")
            .setAutoLoad(true)
            .build();

    public static final Dependency TOML4J = Dependency
            .builder("TOML4J")
            .setGroupId("com{}moandjiezana{}toml")
            .setArtifactId("toml4j")
            .setVersion("0.7.2")
            .setChecksum("9UdeY+fonl22IiNImux6Vr0wNUN3IHehfCy1TBnKOiA=")
            .setAutoLoad(true)
            .build();

    public static final Dependency CONFIGURATE_TOML = Dependency
            .builder("CONFIGURATE_TOML")
            .setGroupId("me{}lucko{}configurate")
            .setArtifactId("configurate-toml")
            .setVersion("3.5")
            .setChecksum("U8p0XSTaNT/uebvLpO/vb6AhVGQDYiZsauSGB9zolPU=")
            .setAutoLoad(true)
            .build();

    public static final Dependency MARIADB_DRIVER = Dependency
            .builder("MARIADB_DRIVER")
            .setGroupId("org{}mariadb{}jdbc")
            .setArtifactId("mariadb-java-client")
            .setVersion("2.2.5")
            .setChecksum("kFfgzoMFrFKirAFh/DgobV7vAu9NhdnhZLHD4/PCddI=")
            .setAutoLoad(true)
            .build();

    public static final Dependency MYSQL_DRIVER = Dependency
            .builder("MYSQL_DRIVER")
            .setGroupId("mysql")
            .setArtifactId("mysql-connector-java")
            .setVersion("5.1.46")
            .setChecksum("MSIIl2HmQD8C6Kge1KLWWi4QKXNGUboA8uqS2SD/ex4=")
            .setAutoLoad(true)
            .build();

    public static final Dependency POSTGRESQL_DRIVER = Dependency
            .builder("POSTGRESQL_DRIVER")
            .setGroupId("org{}postgresql")
            .setArtifactId("postgresql")
            .setVersion("9.4.1212")
            .setChecksum("DLKhWL4xrPIY4KThjI89usaKO8NIBkaHc/xECUsMNl0=")
            .setAutoLoad(true)
            .build();

    public static final Dependency MONGODB_DRIVER = Dependency
            .builder("MONGODB_DRIVER")
            .setGroupId("org.mongodb")
            .setArtifactId("mongo-java-driver")
            .setVersion("3.7.1")
            .setChecksum("yllBCqAZwWCNUoMPR0JWilqhVA46+9F47wIcnYOcoy4=")
            .setAutoLoad(true)
            .build();

    public static final Dependency H2_DRIVER = Dependency
            .builder("H2_DRIVER")
            .setGroupId("com.h2database")
            .setArtifactId("h2")
            .setVersion("1.4.197")
            .setChecksum("N/UhbhSvJ3KTDf+bhzQ1PwqA6Juj8z4GVEHeZTfF6EI=")
            .setAutoLoad(true)
            .build();

    public static final Dependency SQLITE_DRIVER = Dependency
            .builder("SQLITE_DRIVER")
            .setGroupId("org.xerial")
            .setArtifactId("sqlite-jdbc")
            .setVersion("3.27.2.1")
            .setChecksum("SxCoOLnNH9btS1lJZsfHsNFcLUxcmrqYzSjshgLraS0")
            .setAutoLoad(true)
            .build();

    public static final Dependency HIKARI = Dependency
            .builder("HIKARI")
            .setGroupId("com{}zaxxer")
            .setArtifactId("HikariCP")
            .setVersion("3.2.0")
            .setChecksum("sAjeaLvYWBH0tujwhg0JZsastPLnX6vUbsIJRWnL7+s=")
            .setAutoLoad(true)
            .build();
}
