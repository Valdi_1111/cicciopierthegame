package org.valdi.cicciopier.menu;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import org.valdi.cicciopier.Game;
import org.valdi.cicciopier.input.listeners.ToggleKeyListener;
import org.valdi.cicciopier.logger.LoggerController;
import org.valdi.cicciopier.window.GameWindow;
import org.valdi.cicciopier.window.GameWindowController;
import org.valdi.cicciopier.Main;
import org.valdi.cicciopier.world.World;

import java.net.URL;

public class MainMenuController {
    private final Main main;

    @FXML
    private ListView<String> listView;

    @FXML
    private Button playButton;
    @FXML
    private Button settingsButton;

    public MainMenuController(Main main) {
        this.main = main;
    }

    @FXML
    private void initialize() {
        /*listView.setCellFactory(lv -> {
            TextFieldListCell<World> cell = new TextFieldListCell<>();
            cell.setConverter(new StringConverter<World>() {
                @Override
                public String toString(World level) {
                    return level.getProperties().getName();
                }

                @Override
                public World fromString(String id) {
                    return main.getGame().getLevel(id).get();
                }
            });
            return cell;
        });*/
        main.getGame().getLevels().forEach(w -> listView.getItems().add(w.getProperties().getName()));
    }

    @FXML
    private void onPlay() {
        String item = listView.getSelectionModel().getSelectedItem();
        if(item == null) {
            return;
        }

        Game game = main.getGame();
        game.getLogger().info("Hai scelto il livello " + item);

        try {
            Stage owner = (Stage) playButton.getScene().getWindow();
            playButton.getScene().getWindow().hide();

            this.startGameWindow(game, owner, item);
            this.startLoggerWindow(game, owner);
        } catch(Exception e) {
            game.getLogger().severe("Failed to start game window!", e);
        }
    }

    private void startGameWindow(Game game, Stage owner, String level) throws Exception {
        URL url = Main.class.getClassLoader().getResource("fxml/game_window.fxml");
        FXMLLoader loader = new FXMLLoader(url);
        loader.setControllerFactory(c -> new GameWindowController(main));

        Stage stage = new Stage();
        stage.initOwner(owner);
        stage.setOnCloseRequest(e -> main.getGame().stopGame());

        stage.getIcons().addAll(owner.getIcons());
        stage.setTitle(game.getName() + " #" + level);
        stage.setScene(new Scene(loader.load()));
        //stage.setResizable(false);
        stage.show();

        GameWindow window = (GameWindow) stage.getScene().lookup("#window");
                game.startGame(level, window);
    }

    private void startLoggerWindow(Game game, Stage owner) throws Exception {
        URL url = Main.class.getClassLoader().getResource("fxml/logger.fxml");
        FXMLLoader loader = new FXMLLoader(url);
        loader.setControllerFactory(c -> new LoggerController(main));

        Stage stage = new Stage();
        stage.initOwner(owner);
        stage.setOnCloseRequest(e -> stage.hide());

        stage.getIcons().addAll(owner.getIcons());
        stage.setTitle(game.getName() + " - Logger");
        stage.setScene(new Scene(loader.load()));
        stage.setResizable(false);

        main.getGame().getKeyController().registerListener(new ToggleKeyListener(KeyCode.L, false) {
            @Override
            protected boolean canToggle(KeyEvent e) {
                return e.isControlDown();
            }

            @Override
            protected void toggleOn() {
                stage.show();
            }

            @Override
            protected void toggleOff() {
                stage.hide();
            }

            @Override
            public boolean isToggled() {
                return stage.isShowing();
            }
        });
    }

    @FXML
    private void onSettings() {
        // Empty atm
    }

}
