package org.valdi.cicciopier;

import com.google.gson.annotations.Expose;

import java.util.Objects;

public class Checkpoint {
    @Expose
    private int id;
    @Expose
    private Location location;

    public Checkpoint(int id, Location location) {
        this.id = id;
        this.location = location;
    }

    public int getId() {
        return id;
    }

    public Location getLocation() {
        return location;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Checkpoint)) {
            return false;
        }
        Checkpoint that = (Checkpoint) o;
        return id == that.id &&
                location.equals(that.location);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, location);
    }

    @Override
    public String toString() {
        return "Checkpoint{" +
                "id=" + id +
                ", location=" + location +
                '}';
    }
}
