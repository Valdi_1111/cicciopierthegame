package org.valdi.cicciopier;

public class GameDetails {
    public static final String NAME = "CiccioPier The Game";
    public static final String VERSION = "0.1-BETA";
    public static final String[] AUTHORS = new String[] {"Valdi_1111", "GJ KnockOut"};

    public static final String DEFAULT_NAMESPACE = "cicciopier";

    public static final int DEFAULT_LIVES = 3;

    public static final boolean SHOW_FPS = true;
    public static final boolean SHOW_TPS = true;
    public static final int TPS = 20;

    public static final double FRICTION = 0.99;
    public static final double GRAVITY = 2.2;

    public static final double JUMP_STRENGTH = 26;
    public static final double SPEED = 10;

    public static int WIDTH = 1280;
    public static int HEIGHT = 720;

    public static int getBlockSize() {
        return WIDTH / 40;
    }

    public static boolean SHOW_BOUNDS = false;
}
