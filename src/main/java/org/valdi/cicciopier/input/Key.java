package org.valdi.cicciopier.input;

public class Key {
    // Creating the keys as simply variables
    public static Key UP = new Key();
    public static Key DOWN = new Key();
    public static Key LEFT = new Key();
    public static Key RIGHT = new Key();

    public boolean down = false;

    /**
     * Toggles the keys current state
     */
    public void toggle() {
        this.down = !down;
    }

    public boolean isDown() {
        return down;
    }

    public void setDown(boolean down) {
        this.down = down;
    }
}
