package org.valdi.cicciopier.input.listeners;

import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public abstract class PressKeyListener extends AbstractKeyListener {

    protected PressKeyListener(KeyCode key) {
        super(key);
    }

    @Override
    public void keyPressed(KeyEvent e) {
        press(e);
    }

    protected abstract void press(KeyEvent e);

}
