package org.valdi.cicciopier.input.listeners;

import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public abstract class ToggleKeyListener extends AbstractKeyListener {
    private boolean toggle;

    protected ToggleKeyListener(KeyCode key, boolean defaultValue) {
        super(key);

        this.toggle = defaultValue;
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if(!canToggle(e)) {
            return;
        }

        if(isToggled()) {
            this.toggleOff();
            return;
        }

        this.toggleOn();
    }

    protected abstract boolean canToggle(KeyEvent e);

    protected void toggleOn() {
        this.toggle = true;
    }

    protected void toggleOff() {
        this.toggle = false;
    }

    public boolean isToggled() {
        return toggle;
    }
}
