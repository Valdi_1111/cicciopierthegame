package org.valdi.cicciopier.input.listeners;

import javafx.scene.input.KeyCode;
import org.valdi.cicciopier.input.IKeyInput;

public interface IKeyListener extends IKeyInput {

    KeyCode getKey();

}
