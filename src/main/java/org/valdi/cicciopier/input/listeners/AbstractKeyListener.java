package org.valdi.cicciopier.input.listeners;

import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public abstract class AbstractKeyListener implements IKeyListener {
    protected final KeyCode key;

    protected AbstractKeyListener(KeyCode key) {
        this.key = key;
    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public KeyCode getKey() {
        return key;
    }

}
