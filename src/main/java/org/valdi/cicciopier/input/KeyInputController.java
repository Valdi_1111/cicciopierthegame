package org.valdi.cicciopier.input;

import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import org.valdi.cicciopier.input.listeners.AbstractKeyListener;
import org.valdi.cicciopier.input.listeners.IKeyListener;

import java.util.*;

public class KeyInputController implements IKeyInput {
    private Map<KeyCode, Key> keyBindings = new HashMap<>();
    private Map<KeyCode, List<IKeyListener>> keyListeners = new HashMap<>();

    // Assigning the variable keys to actual letters
    public KeyInputController() {
        bind(KeyCode.W, Key.UP);
        bind(KeyCode.A, Key.LEFT);
        bind(KeyCode.S, Key.DOWN);
        bind(KeyCode.D, Key.RIGHT);

        bind(KeyCode.UP, Key.UP);
        bind(KeyCode.LEFT, Key.LEFT);
        bind(KeyCode.DOWN, Key.DOWN);
        bind(KeyCode.RIGHT, Key.RIGHT);
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if(isKeyBinded(e.getCode())) {
            keyBindings.get(e.getCode()).setDown(true);
            return;
        }

        for(IKeyListener l : keyListeners.getOrDefault(e.getCode(), Collections.emptyList())) {
            l.keyPressed(e);
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if(isKeyBinded(e.getCode())) {
            keyBindings.get(e.getCode()).setDown(false);
            return;
        }

        for(IKeyListener l : keyListeners.getOrDefault(e.getCode(), Collections.emptyList())) {
            l.keyReleased(e);
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
        for(IKeyListener l : keyListeners.getOrDefault(e.getCode(), Collections.emptyList())) {
            l.keyTyped(e);
        }
    }

    public boolean isKeyBinded(KeyCode code) {
        return keyBindings.containsKey(code);
    }


    public void bind(KeyCode code, Key key) {
        keyBindings.put(code, key);
    }

    public void releaseAll() {
        for (Key key : keyBindings.values()) {
            key.setDown(false);
        }
    }

    public void registerListener(AbstractKeyListener listener) {
        KeyCode key = listener.getKey();
        if(!keyListeners.containsKey(key)) {
            keyListeners.put(key, new ArrayList<>());
        }

        keyListeners.get(key).add(listener);
    }
}
