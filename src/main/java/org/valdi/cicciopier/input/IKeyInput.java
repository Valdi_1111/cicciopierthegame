package org.valdi.cicciopier.input;

import javafx.scene.input.KeyEvent;

public interface IKeyInput {

    void keyPressed(KeyEvent e);

    void keyReleased(KeyEvent e);

    void keyTyped(KeyEvent e);

}

