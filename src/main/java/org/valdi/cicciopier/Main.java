package org.valdi.cicciopier;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.valdi.cicciopier.menu.MainMenuController;

import java.security.AccessControlException;

public class Main extends Application {
    private static Logger LOGGER;
    private static Game INSTANCE;
    private static Stage STAGE;

    @Override
    public void start(Stage stage) throws Exception{
        STAGE = stage;

        try {
            System.setProperty("prism.verbose", "true");
            System.setProperty("prism.dirtyopts", "false");
            //System.setProperty("javafx.animation.fullspeed", "true");
            System.setProperty("javafx.animation.pulse", "10");
        } catch (AccessControlException ignored) {}

        FXMLLoader loader = new FXMLLoader(Main.class.getClassLoader().getResource("fxml/main_menu.fxml"));
        loader.setControllerFactory(c -> new MainMenuController(this));
        Parent root = loader.load();
        stage.setOnCloseRequest((e) -> {
            INSTANCE.stopGame();
        });

        Image icon = new Image("assets/cicciopier/textures/icon.png");
        stage.getIcons().add(icon);
        stage.setTitle(INSTANCE.getName());
        stage.setScene(new Scene(root));
        stage.show();
    }

    public static void main(String[] args) {
        LOGGER = LogManager.getLogger("Main");

        LOGGER.info("Starting game...");
        INSTANCE = new Game(args);

        LOGGER.info("Starting application...");
        launch(args);
    }

    public Game getGame() {
        return INSTANCE;
    }
}
