package org.valdi.cicciopier.assets;

import javafx.scene.image.Image;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.valdi.cicciopier.Game;
import org.valdi.cicciopier.key.GameKey;

import java.io.File;
import java.net.URL;

public class Texture implements ITexture {
    private static final Logger LOGGER = LogManager.getLogger("Textures");

    private final GameKey key;
    private final String path;
    private Image texture;

    public Texture(GameKey key, String path) {
        this.key = key;
        this.path = path;
    }

    @Override
    public String getPath() {
        return path;
    }

    @Override
    public boolean load(Game game, File resourcePack) {
        try {
            LOGGER.info("Loading texture at " + path);
            if(resourcePack != null && !resourcePack.exists()) {
                File file = new File(resourcePack, path);
                this.texture = new Image(file.toURI().toString());
                return true;
            }

            URL url = Game.class.getClassLoader().getResource(path);
            this.texture = new Image(url.toURI().toString());
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public Image getTexture() {
        return texture;
    }

    @Override
    public GameKey getKey() {
        return key;
    }
}
