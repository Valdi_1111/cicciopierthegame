package org.valdi.cicciopier.assets;

import org.valdi.cicciopier.Game;
import org.valdi.cicciopier.registries.RegistryElement;

import java.io.File;

public interface ISound<T> extends RegistryElement {

    String getPath();

    boolean load(Game game, File resourcePack);

    T getSound();
}
