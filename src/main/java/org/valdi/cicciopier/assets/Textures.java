package org.valdi.cicciopier.assets;

import org.valdi.cicciopier.key.TexturesKey;
import org.valdi.cicciopier.registries.AutoRegister;


public class Textures implements AutoRegister {
    public static final Texture BLOCK_GROUND = new Texture(new TexturesKey("block.ground"), "assets/cicciopier/textures/block/ground.png");
    public static final Texture BLOCK_DIRT = new Texture(new TexturesKey("block.dirt"), "assets/cicciopier/textures/block/dirt.png");
    public static final Texture BLOCK_GRASS = new Texture(new TexturesKey("block.grass"), "assets/cicciopier/textures/block/grass.png");
    public static final Texture BLOCK_WALL = new Texture(new TexturesKey("block.wall"), "assets/cicciopier/textures/block/wall.png");
    public static final Texture BLOCK_WALL_UNSTABLE = new Texture(new TexturesKey("block.wall_unstable"), "assets/cicciopier/textures/block/wall_unstable.png");
    public static final Texture BLOCK_COIN = new Texture(new TexturesKey("block.coin"), "assets/cicciopier/textures/item/coin.png");

    public static final Texture BLOCK_BUTTON = new Texture(new TexturesKey("block.button"), "assets/cicciopier/textures/block/button.png");
    public static final Texture BLOCK_BUTTON_PRESSED = new Texture(new TexturesKey("block.button_pressed"), "assets/cicciopier/textures/block/button_pressed.png");

    public static final Texture ENTITY_PLAYER_STILL = new Texture(new TexturesKey("entity.player.still"), "assets/cicciopier/textures/entity/player_still.png");
    public static final Texture ENTITY_PLAYER_MOVE_1 = new Texture(new TexturesKey("entity.player.move.1"), "assets/cicciopier/textures/entity/player_move_1.png");
    public static final Texture ENTITY_PLAYER_MOVE_2 = new Texture(new TexturesKey("entity.player.move.2"), "assets/cicciopier/textures/entity/player_move_2.png");

    public static final Texture ENTITY_ANANAS_STILL = new Texture(new TexturesKey("entity.ananas.still"), "assets/cicciopier/textures/entity/ananas_still.png");
    public static final Texture ENTITY_ANANAS_MOVE = new Texture(new TexturesKey("entity.ananas.move"), "assets/cicciopier/textures/entity/ananas_move.png");

    public static final Texture ENTITY_CIPALLOTTOLA_STILL = new Texture(new TexturesKey("entity.cipallottola.still"), "assets/cicciopier/textures/entity/cipallottola_still.png");
    public static final Texture ENTITY_CIPALLOTTOLA_MOVE = new Texture(new TexturesKey("entity.cipallottola.move"), "assets/cicciopier/textures/entity/cipallottola_move.png");

    public static final Texture ENTITY_FINOCCHIO_NINJIA_STILL = new Texture(new TexturesKey("entity.finocchio_ninjia.still"), "assets/cicciopier/textures/entity/finocchio_ninjia_still.png");
    public static final Texture ENTITY_FINOCCHIO_NINJIA_MOVE = new Texture(new TexturesKey("entity.finocchio_ninjia.move"), "assets/cicciopier/textures/entity/finocchio_ninjia_move.png");
    public static final Texture ENTITY_SHURIKEN = new Texture(new TexturesKey("entity.shuriken"), "assets/cicciopier/textures/entity/shuriken.png");

    public static final Texture ICON_HEART = new Texture(new TexturesKey("misc.heart"), "assets/cicciopier/textures/misc/heart.png");

    public static final Texture ICON_CHECKPOINT = new Texture(new TexturesKey("misc.checkpoint"), "assets/cicciopier/textures/misc/checkpoint.png");
    public static final Texture ICON_ENDPOINT = new Texture(new TexturesKey("misc.endpoint"), "assets/cicciopier/textures/misc/endpoint.png");

    public static final Texture MISC_LOADING_0 = new Texture(new TexturesKey("misc.loading.0"), "assets/cicciopier/textures/misc/loading_0.png");
    public static final Texture MISC_LOADING_1 = new Texture(new TexturesKey("misc.loading.1"), "assets/cicciopier/textures/misc/loading_1.png");
    public static final Texture MISC_LOADING_2 = new Texture(new TexturesKey("misc.loading.2"), "assets/cicciopier/textures/misc/loading_2.png");
    public static final Texture MISC_LOADING_3 = new Texture(new TexturesKey("misc.loading.3"), "assets/cicciopier/textures/misc/loading_3.png");
}
