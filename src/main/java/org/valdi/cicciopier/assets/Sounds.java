package org.valdi.cicciopier.assets;

import org.valdi.cicciopier.key.SoundKey;
import org.valdi.cicciopier.registries.AutoRegister;


public class Sounds implements AutoRegister {
    public static final Music MUSIC_LEVEL = new Music(new SoundKey("music.level"), "assets/cicciopier/sounds/music/level.wav");

    public static final Sound PLAYER_JUMP = new Sound(new SoundKey("entity.player.jump"), "assets/cicciopier/sounds/entity/player_jump.wav");

}
