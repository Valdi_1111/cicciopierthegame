package org.valdi.cicciopier.assets;

import javafx.scene.media.AudioClip;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.valdi.cicciopier.Game;
import org.valdi.cicciopier.key.GameKey;

import java.io.File;
import java.net.URL;

public class Sound implements ISound<AudioClip> {
    private static final Logger LOGGER = LogManager.getLogger("Sounds");

    private final GameKey key;
    private final String path;
    private AudioClip sound;

    public Sound(GameKey key, String path) {
        this.key = key;
        this.path = path;
    }

    @Override
    public String getPath() {
        return path;
    }

    @Override
    public boolean load(Game game, File resourcePack) {
        try {
            LOGGER.info("Loading sound at " + path);
            if(resourcePack != null && !resourcePack.exists()) {
                File file = new File(resourcePack, path);
                this.sound = new AudioClip(file.toURI().toString());
                return true;
            }

            URL url = Game.class.getClassLoader().getResource(path);
            this.sound = new AudioClip(url.toURI().toString());
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public AudioClip getSound() {
        return sound;
    }

    @Override
    public GameKey getKey() {
        return key;
    }
}
