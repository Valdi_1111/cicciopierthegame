package org.valdi.cicciopier.assets;

import javafx.scene.image.Image;
import org.valdi.cicciopier.Game;
import org.valdi.cicciopier.registries.RegistryElement;

import java.io.File;

public interface ITexture extends RegistryElement {

    String getPath();

    boolean load(Game game, File resourcePack);

    Image getTexture();

}
