package org.valdi.cicciopier.registries;

import org.valdi.cicciopier.assets.ISound;

public class SoundsRegistry extends AbstractRegistry<ISound> {

    public boolean register(ISound sound) {
        return register(sound.getKey(), sound);
    }

}
