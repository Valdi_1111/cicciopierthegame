package org.valdi.cicciopier.registries;

import org.valdi.cicciopier.key.GameKey;

public interface RegistryElement {

    GameKey getKey();

}
