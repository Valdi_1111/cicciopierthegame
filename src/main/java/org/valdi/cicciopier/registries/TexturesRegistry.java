package org.valdi.cicciopier.registries;

import org.valdi.cicciopier.assets.ITexture;

public class TexturesRegistry extends AbstractRegistry<ITexture> {

    public boolean register(ITexture texture) {
        return register(texture.getKey(), texture);
    }

}
