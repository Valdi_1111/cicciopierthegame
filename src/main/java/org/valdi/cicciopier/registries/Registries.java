package org.valdi.cicciopier.registries;

public class Registries {
    public static final BlocksRegistry BLOCKS = new BlocksRegistry();
    public static final EntitiesRegistry ENTITIES = new EntitiesRegistry();

    public static final TexturesRegistry TEXTURES = new TexturesRegistry();
    public static final SoundsRegistry SOUNDS = new SoundsRegistry();

}
