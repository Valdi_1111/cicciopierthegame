package org.valdi.cicciopier.registries;

import org.valdi.cicciopier.key.GameKey;

import java.util.Optional;
import java.util.Set;

public interface IRegistry<K> extends Iterable<K> {

    boolean register(GameKey key, K value);

    boolean isRegistered(GameKey key);

    GameKey getKey(K value);

    K get(GameKey key);

    Optional<K> getOptional(GameKey key);

    Set<GameKey> keySet();

    boolean isEmpty();

    boolean isLocked();

    void lock();

}
