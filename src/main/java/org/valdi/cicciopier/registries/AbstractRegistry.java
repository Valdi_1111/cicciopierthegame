package org.valdi.cicciopier.registries;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import org.valdi.cicciopier.key.GameKey;

import java.util.Collections;
import java.util.Iterator;
import java.util.Optional;
import java.util.Set;

public class AbstractRegistry<K> implements IRegistry<K> {
    private final BiMap<GameKey, K> elements = HashBiMap.create();
    private boolean locked = false;

    @Override
    public boolean register(GameKey key, K value) {
        if(locked) {
            throw new IllegalStateException("Cannot register element to " + this.getClass().getSimpleName() + ", registry is locked!");
        }

        if(isRegistered(key)) {
            return false;
        }

        elements.put(key, value);
        return true;
    }

    @Override
    public boolean isRegistered(GameKey key) {
        return elements.containsKey(key);
    }

    @Override
    public GameKey getKey(K value) {
        return elements.inverse().get(value);
    }

    @Override
    public K get(GameKey key) {
        return elements.get(key);
    }

    @Override
    public Optional<K> getOptional(GameKey key) {
        return Optional.ofNullable(elements.get(key));
    }

    @Override
    public Iterator<K> iterator() {
        return elements.values().iterator();
    }

    @Override
    public Set<GameKey> keySet() {
        return Collections.unmodifiableSet(elements.keySet());
    }

    @Override
    public boolean isEmpty() {
        return elements.isEmpty();
    }

    @Override
    public boolean isLocked() {
        return locked;
    }

    @Override
    public void lock() {
        this.locked = true;
    }
}
