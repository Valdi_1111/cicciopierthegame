package org.valdi.cicciopier.registries;

import org.valdi.cicciopier.block.Block;

public class BlocksRegistry extends AbstractRegistry<Block> {

    public boolean register(Block block) {
        return register(block.getKey(), block);
    }

}
