package org.valdi.cicciopier;

import com.sun.javafx.perf.PerformanceTracker;
import javafx.animation.AnimationTimer;
import javafx.geometry.VPos;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import org.valdi.cicciopier.assets.Textures;
import org.valdi.cicciopier.window.GameWindow;

import static org.valdi.cicciopier.GameDetails.*;

public class RenderTimer extends AnimationTimer implements IRenderizable {
    private final Game game;
    private final GameWindow window;
    private final PerformanceTracker tracker;

    public RenderTimer(Game game, GameWindow window) {
        super();

        this.game = game;
        this.window = window;
        this.tracker = PerformanceTracker.getSceneTracker(window.getScene());
    }

    private long lastFpsUpdate = 0L;
    private float fps = 0F;

    @Override
    public void handle(long currentNanoTime) {
        GraphicsContext g = window.getGraphicsContext2D();
        g.clearRect(0D, 0D, WIDTH, HEIGHT);

        if(lastFpsUpdate == 0L || currentNanoTime > lastFpsUpdate + 1000000000L) {
            this.lastFpsUpdate = currentNanoTime;

            this.fps = tracker.getAverageFPS();
            tracker.resetAverageFPS();
        }

        switch (game.getState()) {
            case RUNNING:
                doRender(g);
                break;

            case PAUSED:
                doRender(g);

                g.setFill(Color.RED);
                g.setTextAlign(TextAlignment.CENTER);
                g.setTextBaseline(VPos.CENTER);
                g.setFont(Font.font(75));
                g.fillText("Pause", WIDTH / 2F, HEIGHT / 2F - 25);
                g.setFont(Font.font(25));
                g.fillText("Press ESC to Resume!", WIDTH / 2F, HEIGHT / 2F + 50);
                break;

            case WON:
                doRender(g);

                g.setFont(Font.font(50));
                g.setFill(Color.RED);
                g.setTextAlign(TextAlignment.CENTER);
                g.setTextBaseline(VPos.CENTER);
                g.fillText("YOU WON!", WIDTH / 2F, HEIGHT / 2F);
                break;

            case DEAD:
            case FULL_DEAD:
                doRender(g);

                g.setFont(Font.font(50));
                g.setFill(Color.RED);
                g.setTextAlign(TextAlignment.CENTER);
                g.setTextBaseline(VPos.CENTER);
                g.fillText("YOU DIED!", WIDTH / 2F, HEIGHT / 2F);
                break;

            case SELECTING_LEVEL:
            case WAITING_RESET:
            case WAITING_FULL_RESET:
            case RESETTING:
                /*g.setFill(Color.BLACK);
                g.setFont(new Font(80));
                g.setTextAlign(TextAlignment.CENTER);
                g.setTextBaseline(VPos.CENTER);
                g.fillText("Loading Level...", window.getWidth() / 2, window.getHeight() / 2);*/
                g.drawImage(Textures.MISC_LOADING_0.getTexture(), 0D, 0D, WIDTH, HEIGHT);
                break;

            case STOPPING:
                break;
        }
    }

    @Override
    public void doRender(GraphicsContext g) {
        game.getPlayingLevel().get().doRender(g);

        g.setFill(Color.BLACK);
        g.setFont(new Font(15));
        g.setTextAlign(TextAlignment.RIGHT);
        g.setTextBaseline(VPos.CENTER);

        String coinsText = String.format("%d x", game.getCoins());
        g.fillText(coinsText, WIDTH - 45, 35);
        g.drawImage(Textures.BLOCK_COIN.getTexture(), WIDTH - 40, 25, 20D, 20D);

        String livesText = String.format("%d x", game.getLives());
        g.fillText(livesText, WIDTH - 45, 65);
        g.drawImage(Textures.ICON_HEART.getTexture(), WIDTH - 40, 55, 20D, 20D);

        game.getPlayer().ifPresent(p -> {
            String hpText = String.format("%.2f hp", p.getHealth());
            g.fillText(hpText, WIDTH - 20, 95);
        });

        g.setFill(Color.RED);
        g.setFont(new Font(15));
        g.setTextAlign(TextAlignment.LEFT);
        g.setTextBaseline(VPos.TOP);
        if(SHOW_FPS) {
            String fpsText = String.format("%d fps", Math.round(fps));
            g.fillText(fpsText, 15, 15);
        }

        if(SHOW_TPS) {
            String fpsText = String.format("%d tps", Math.round(game.getLastTps()));
            g.fillText(fpsText, 15, 40);
        }
    }

    public float getFps() {
        return fps;
    }

    public GameWindow getWindow() {
        return window;
    }
}
