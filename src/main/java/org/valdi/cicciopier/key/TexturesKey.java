package org.valdi.cicciopier.key;

public class TexturesKey extends GameKey {
    public TexturesKey(String key) {
        super("textures/" + key);
    }

    public TexturesKey(String namespace, String key) {
        super(namespace, "textures/" + key);
    }
}
