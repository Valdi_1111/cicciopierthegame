package org.valdi.cicciopier.key;

public class SoundKey extends GameKey {
    public SoundKey(String key) {
        super("sounds/" + key);
    }

    public SoundKey(String namespace, String key) {
        super(namespace, "sounds/" + key);
    }
}
