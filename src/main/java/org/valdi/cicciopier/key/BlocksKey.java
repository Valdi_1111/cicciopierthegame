package org.valdi.cicciopier.key;

public class BlocksKey extends GameKey {
    public BlocksKey(String key) {
        super("blocks/" + key);
    }

    public BlocksKey(String namespace, String key) {
        super(namespace, "blocks/" + key);
    }
}
