package org.valdi.cicciopier.key;

public class EntitiesKey extends GameKey {
    public EntitiesKey(String key) {
        super("entities/" + key);
    }

    public EntitiesKey(String namespace, String key) {
        super(namespace, "entities/" + key);
    }
}
