package org.valdi.cicciopier.key;

import java.util.Objects;

import static org.valdi.cicciopier.GameDetails.DEFAULT_NAMESPACE;

public class GameKey {

    private String namespace;
    private String key;

    public GameKey(String key) {
        this(DEFAULT_NAMESPACE, key);
    }

    public GameKey(String namespace, String key) {
        this.namespace = namespace;
        this.key = key;
    }

    public String getNamespace() {
        return namespace;
    }

    public String getKey() {
        return key;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof GameKey)) {
            return false;
        }
        GameKey gameKey = (GameKey) o;
        return namespace.equals(gameKey.namespace) &&
                key.equals(gameKey.key);
    }

    @Override
    public int hashCode() {
        return Objects.hash(namespace, key);
    }

    @Override
    public String toString() {
        return namespace + ":" + key;
    }
}
