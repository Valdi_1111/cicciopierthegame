package org.valdi.cicciopier;

/*public class GameThread extends Thread implements ITickable {
    private final Game game;

    private int ticks;

    public GameThread(Game game) {
        super("Game Thread");

        this.game = game;
        this.ticks = 0;
    }

    @Override
    public void run() {
        while (game.isRunning()) {
            ++ticks;
            doTick();

            game.getScheduler().tick(ticks);

            try {
                Thread.sleep(50L);
            } catch (InterruptedException ignored) {
                // Nothing here
            }
        }
    }

    @Override
    public void doTick() {
        game.getPlayingLevel().doTick();
    }

    public int getCurrentTicks() {
        return ticks;
    }

    public int getLastTps() {
        return 0;
    }
}*/

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import org.valdi.cicciopier.assets.Sounds;

import static org.valdi.cicciopier.GameDetails.TPS;

public class GameThread extends Thread implements ITickable {
    private final Game game;
    private MediaPlayer music;

    private int lastTps;
    private int currentTps;
    private int ticks;

    public GameThread(Game game) {
        super("Game Thread");

        this.game = game;

        Media sound = Sounds.MUSIC_LEVEL.getSound();
        if(sound != null) {
            this.music = new MediaPlayer(sound);
        }
    }

    @Override
    public void run() {
        if(music != null) {
            music.setVolume(0.1F);
            music.play();
        }

        // Tick counter variable
        long lastTime = System.nanoTime();
        // Nanoseconds per Tick
        final double nsPerTick = 1000000000D / TPS;

        this.lastTps = TPS;
        this.currentTps = 0;
        this.ticks = 0;

        long lastUpdate = System.currentTimeMillis();
        double delta = 0;
        while (game.getState() != GameState.STOPPING) {
            long now = System.nanoTime();
            delta += (now - lastTime) / nsPerTick;
            lastTime = now;
            // If it should tick it does this
            while (delta >= 1) {
                currentTps++;
                ticks++;

                if(game.getState() == GameState.RUNNING) {
                    tickGame();
                }

                if(game.getState() == GameState.WAITING_RESET) {
                    game.resetLevel(false);
                    game.getScheduler().cancelTasks();

                    this.lastTps = TPS;
                    this.currentTps = 0;
                    this.ticks = 0;
                }

                if(game.getState() == GameState.WAITING_FULL_RESET) {
                    game.resetLevel(true);
                    game.getScheduler().cancelTasks();

                    this.lastTps = TPS;
                    this.currentTps = 0;
                    this.ticks = 0;
                }

                delta -= 1;
            }
            if (lastUpdate < System.currentTimeMillis() - 1000) {
                this.lastTps = currentTps;
                this.currentTps = 0;
                lastUpdate = System.currentTimeMillis();
            }
        }
    }

    public void tickGame() {
        if (music != null && (music.getStatus() == MediaPlayer.Status.STOPPED ||
                music.getStatus() == MediaPlayer.Status.DISPOSED ||
                music.getStatus() == MediaPlayer.Status.STALLED)) {
            music.play();
        }

        doTick();
        game.getScheduler().tick(ticks);
    }

    @Override
    public void doTick() {
        game.getPlayingLevel().get().doTick();
    }

    public int getCurrentTicks() {
        return ticks;
    }

    public int getLastTps() {
        return lastTps;
    }
}
